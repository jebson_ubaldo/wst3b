<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="style/home.css">

    <!-- fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body {
          background: #F2F6FE;
        }
        #assignment {
          margin-top: 8px;
        }
        #profile {
          border-radius: 50%;
        }
    </style>

    <title>Home</title>
</head>
<body>
    
    <!-- Navbar -->
    <nav class="navbar navbar-expand-xl bg-light">
      <div class="container-lg">
        <a class="navbar-brand" href="#"><h2 id="assignment"> Assignment 2</h2></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">

            
            </li>

            <li class="nav-item">
              <a class="nav-link" href="logout.php">Logout</a>
            </li>
          </ul>
        </div>

      </div>
    </nav>

   
    <!-- Write your code below this-->
    <div class="container-fluid brgy-bg d-flex flex-column justify-content-center">
      <h1 class="text-center rm-display-1 rm-text-semi-bold text-light p-2">Welcome Facebook</h1>
    </div>

    <!-- Footer -->
    <footer class="bg-light text-center text-lg-start">
        <div id="left" class="text-center p-3" style="background: #F2F6FE">
          <div>
            <a href="" class="me-4 text-reset">
              <i class="fa fa-facebook"></i>
            </a>

            <a href="" class="me-4 text-reset">
              <i class="fa fa-twitter"></i>
            </a>

            <a href="" class="me-4 text-reset">
              <i class="fa fa-google"></i>
            </a>

            <a href="" class="me-4 text-reset">
              <i class="fa fa-instagram"></i>
            </a>

            <a href="" class="me-4 text-reset">
              <i class="fa fa-linkedin"></i>
            </a>

            <a href="" class="me-4 text-reset">
              <i class="fa fa-github"></i>
            </a>
          </div>
        </div>

        <div id="right" class="text-center p-3" style="background: #F2F6FE">
          Made with &#128153; by Jebson Ubaldo
        </div>
    </footer>

      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>