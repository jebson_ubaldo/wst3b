<?php
    //Include Configuration File
    include('config-google.php');

    $login_button = '';

    //This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
    if(isset($_GET["code"]))
    {
    //It will Attempt to exchange a code for an valid authentication token.
    $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

    //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
    if(!isset($token['error']))
    {
    //Set the access token used for requests
    $google_client->setAccessToken($token['access_token']);

    //Store "access_token" value in $_SESSION variable for future use.
    $_SESSION['access_token'] = $token['access_token'];

    //Create Object of Google Service OAuth 2 class
    $google_service = new Google_Service_Oauth2($google_client);

    //Get user profile data from google
    $data = $google_service->userinfo->get();

    //Below you can find Get profile data and store into $_SESSION variable
    if(!empty($data['given_name']))
    {
    $_SESSION['user_first_name'] = $data['given_name'];
    }

    if(!empty($data['family_name']))
    {
    $_SESSION['user_last_name'] = $data['family_name'];
    }

    if(!empty($data['email']))
    {
    $_SESSION['user_email_address'] = $data['email'];
    }

    if(!empty($data['gender']))
    {
    $_SESSION['user_gender'] = $data['gender'];
    }

    if(!empty($data['picture']))
    {
    $_SESSION['user_image'] = $data['picture'];
    }
    }
    }

    //This is for check user has login into system by using Google account, if User not login into system then it will execute if block of code and make code for display Login link for Login using Google account.
    if(!isset($_SESSION['access_token']))
    {
    //Create a URL to obtain user authorization
    $login_button = '<a href="'.$google_client->createAuthUrl().'"><i class="fab fa-google-plus-square"><span>Google</span></i></a>';
    }
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <title>Elective 1</title>
</head>
<body>
    <div class="container">
        <header>Login Form</header>
            <form>
                <div class="input-field">
                    <input type="text" required>
                    <label>Email or Username</label>
                </div>

                <div class="input-field">
                    <input class="pswrd" type="password" required>
                    <label>Password</label>
                </div>

                <div class="button">
                    <div class="inner"></div>
                    <button>LOGIN</button>
                </div>
            </form>

        <div class="auth">
            Or login with
        </div>

        <div class="links">
            <div class="facebook">
                <i class="fab fa-facebook"><span>Facebook</span></i></a>
            </div>

            <div class="google">
                <?php
                    if($login_button == '')
                    {
                        header('location:home-google.php');
                    }
                    else
                    {
                        echo '<div align="center">'.$login_button . '</div>';
                    }
                ?>
            </div>
        </div>

        <div class="signup">
            Made with &#128153; by Jebson Ubaldo <br>
        </div>
    </div>
</body>
</html>