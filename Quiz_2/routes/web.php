<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route:: view('/item', 'item');
Route:: view('/customer', 'customer');
Route:: view('/order', 'order');
Route:: view('/orderdetails', 'orderdetails');


Route::get('/item', function () {
    return view('item');
});


Route::get('/customer', function () {
    return view('customer');
});


Route::get('/order', function () {
    return view('order');
});


Route::get('/orderdetails', function () {
    return view('orderdetails');
});