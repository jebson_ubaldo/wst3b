<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jebson Ubaldo</title>

    <!-- CSS -->
    <link rel='stylesheet' href='/css/style.css'>

    <!-- Fontawesome -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>  

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
    </style>
</head>
<body>
    <!-- Home -->
    <div class="home" id="home">
        <nav>
            <h1 class="logo-name">UBALDO.dev</h1>
            <ul>
                <li><a href="#home">Home</a></li>
                <li><a href="#aboutme">About Me</a></li>
                <li><a href="#getintouch">Get In Touch</a></li>
                <li><a href="#registration">Registration</a></li>
                <li><a href="#login" class="button">Login</a></li>
            </ul>
        </nav>
        <div class="home-info">
            <h1>Hello.</h1>
            <h1>I am <span>Jebson Ubaldo</span></h1>
            <p>I am a developer specializing in building<br> systems and applications based in Philippines.</p>
            <a href="/assets/Jebson Ubaldo Resume.pdf" download class="button">DOWNLOAD RESUME</a>
        </div>
        <div class="img-box">
            <img src="/img/img1.png" class="back-img">
        </div>
        <div class="social-media">
            <a href="https://web.facebook.com/jebsonoreniaubaldo/"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="https://www.instagram.com/jebson_ubaldo/"><i class="fa fa-instagram"></i></a>
            <a href="#"><i class="fa fa-google"></i></a>
            <a href="https://www.linkedin.com/in/jebson-ubaldo-64b132209/"><i class="fa fa-linkedin"></i></a>
            <a href="https://github.com/JebsonUbaldo"><i class="fa fa-github"></i></a>
        </div>
    </div>



    <!-- About Us -->
    <div class="aboutme" id="aboutme">
        <div class="aboutme-info">
            <h1><span>About</span> Me</h1>
            <p>I am a student from Pangasinan State University, Urdaneta Campus<br>
               taking up Bachelor of Science in Information Technology.<br>
               I live in Lipay, Villasis, Pangasinan.<br>
               My ambition is to become a successful Web Developer.</p>

            <p>The following are my skills:</p>
            <a href="#" class="button">HTML</a>
            <a href="#" class="button">CSS</a>
            <a href="#" class="button">Javascript</a>
            <a href="#" class="button">Bootstrap</a>
            <a href="#" class="button">JQuery</a>
            <a href="#" class="button">PHP</a>
            <a href="#" class="button">MySQL</a>
        </div>
        <!--<div class="aboutme-img">
            <img src="./img/img1.png" class="back-img">
        </div>-->
    </div>



    <!-- Get In Touch -->
    <div class="getintouch" id="getintouch">  
        <div class="getintouch-info">
            <h1>Get In <span>Touch</span></h1>
            <p><i class="fa fa-envelope-o"></i> Email: ubaldojebson@gmail.com</p>
            <p><i class="fa fa-phone"></i> Mobile: 09456714294</p>
            <p><i class="fa fa-facebook"></i> Facebook: @ubaldojebsonorenia</p>
            <p><i class="fa fa-instagram"></i> Instagram: @ubaldojebsonorenia</p>
        </div>
        <div class="img-box">
            <img src="/img/img2.png" class="back-img">
        </div>
    </div>



    <!-- Registration -->
    <div class="registration" id="registration">
        <div class="registration-info">
            <h1><span>Registration </span>Form</h1>
            <div class="registration-container">
                <p><i class="fa fa-envelope-o"></i> Email Address: <input type="text"></p>
                <p><i class="fa fa-user"></i> Username: <input type="text"></p>
                <p><i class="fa fa-lock"></i> Password: <input type="text"></p>
                <p><a href="#" class="button">REGISTER</a></p>
            </div>
        </div>
    </div>



    <!-- Login -->
    <div class="login" id="login">
        <div class="login-info">
            <h1>Login <span>Form</span></h1>
            <div class="login-container">
                <p><i class="fa fa-envelope-o"></i> Email Address: <input type="text"></p>
                <p><i class="fa fa-lock"></i> Password: <input type="text"></p>
                <p><a href="#" class="button">LOGIN</a></p>
            </div>
        </div>
        <div class="img-box">
            <img src="/img/img3.png" class="back-img">
        </div>
    </div>
</body>
</html>