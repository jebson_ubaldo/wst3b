<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/customer/{custId}/{custName}/{custAddress}/{custAge?}', function($custId,$custName,$custAddress,$custAge=null){
  return view ('customer', ['custId'=>$custId, 'custName'=>$custName, 'custAddress'=>$custAddress, 'custAge'=>$custAge]);
});

Route::get('/item/{itemNo}/{itemName}/{itemPrice}', function($itemNo,$itemName,$itemPrice){
  return view ('item', ['itemNo'=>$itemNo, 'itemName'=>$itemName, 'itemPrice'=>$itemPrice]);
});

Route::get('/order/{custId}/{custName}/{orderNo}/{orderDate}', function($custId,$custName,$orderNo,$orderDate){
  return view ('order', ['custId'=>$custId, 'custName'=>$custName, 'orderNo'=>$orderNo, 'orderDate'=>$orderDate]);
});

Route::get('/orderDetails/{transNo}/{orderNo}/{itemId}/{itemName}/{itemPrice}/{qty}/{receiptNumber?}', 
  function($transNo,$orderNo,$itemId,$itemName,$itemPrice,$qty,$receiptNumber=null){
    return view ('orderDetails', ['transNo'=>$transNo, 'orderNo'=>$orderNo, 'itemId'=>$itemId, 
                 'itemName'=>$itemName, 'itemPrice'=>$itemPrice, 'qty'=>$qty, 'receiptNumber'=>$receiptNumber,'totalPrice'=>$itemPrice * $qty]);
});