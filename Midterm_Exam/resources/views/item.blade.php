<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Item</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Internal CSS -->
    <style>
        body {
            background-color: #edf2fc;
        }

        h1 {
            font-size: 44px;
            font-weight: bold;
            color: #6C6DF5;
        }

        i {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <section class="item" id="item"> 
        <div class="container-lg">
            <div class="row min-vh-100 align-items-center align-content-center">   
                
                <div class="col-md-6 order-md-first mt-5 mt-md-0 ">
                    <div class="item-text">
                        <h1>item</h1>
                        <p>/ˈʌɪtəm/</p><br>
                        <p><i>noun</i></p>
                        <p>1. a piece of news or information. </p>
                        <p>2. an entry in an account. </p>
                        <p><i>adverb</i></p>
                        <p>1. used to introduce each item in a list. </p>
                    </div>
                </div>


                <div class="col-md-6 mt-5 mt-md-0">
                    <div class="item-item shadow-sm p-4 rounded bg-white h-100">
                        <div class="form-group pt-4 pb-2">
                            <label for="itemNo" class="col-sm-4 control-label">Item No</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="itemNo" name="itemNo" value="{{$itemNo}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-2">
                            <label for="itemName" class="col-sm-4 control-label">Item Name</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="itemName" name="itemName" value="{{$itemName}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-4">
                            <label for="itemPrice" class="col-sm-4 control-label">Item Price</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="itemPrice" name="itemPrice" value="{{$itemPrice}}" required>
                            </div>
                        </div>
                    </div>
                    <footer class="bg-light text-center text-lg-start">
                        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                          Made with &#128153; by Jebson Ubaldo BSIT-3B
                        </div>
                    </footer>
                </div>

            </div>
        </div>
    </section>
</body>
</html>