<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AboutSectionController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AwardsSectionController;
use App\Http\Controllers\BookCategoryController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\BorrowBooksController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\GallerySectionController;
use App\Http\Controllers\ElectronicResourcesController;
use App\Http\Controllers\NewsSectionController;
use App\Http\Controllers\ProgramsSectionController;
use App\Http\Controllers\ServicesSectionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});



//Login Authentication Start
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
//Login Authentication End



//Website Start
Route::get('/',[WebsiteController::class, 'manageWebsite']);
Route::post('/submitContactForm',[WebsiteController::class, 'submitContactForm']);
Route::get('/qr-code',[WebsiteController::class, 'scanQrCode']);
//Website End



//User List of Books Start
Route::get('/books',[UserController::class, 'userBooks']);
Route::get('/book-category',[UserController::class, 'userBookCategory']);
//User List of Books Start



//User Electronic Resources Start
Route::get('/electronic-resources',[UserController::class, 'userElectronicResources']);
//User Electronic Resources End



//User Borrow Book Start
Route::get('/borrow-books/{id}',[UserController::class, 'userBorrowBooks']);
Route::post('/borrowBook',[UserController::class, 'borrowBook']);
Route::get('cancelBorrowBook/{id}',[UserController::class, 'cancelBorrowBook']);
Route::get('/borrow-book-successful',[UserController::class, 'userBorrowBookSuccessful']);
Route::get('/borrow-book-cancel',[UserController::class, 'userBorrowBookCancel']);
//User Borrow Book End



//User Appointment Page Start
Route::get('/appointment/{id}',[UserController::class, 'AppointmentPage']);
Route::post('/create',[UserController::class, 'insert']);
Route::get('/appointment-update/{id}',[UserController::class, 'showUserAppointment']);
Route::post('/appointment-update/{id}',[UserController::class, 'editUserAppointment']);
Route::get('/appointment-successful',[UserController::class, 'userAppointmentSuccessful']);
Route::get('/appointment-cancel',[UserController::class, 'userAppointmentCancel']);
Route::get('cancel/{id}',[UserController::class, 'cancelUserAppointment']);
//User Appointment Page End



//Admin List of Books Start
Route::get('/admin/books',[BooksController::class, 'adminBooks']);
Route::post('/addBook',[BooksController::class, 'addBook']);
Route::get('/books-update/{id}',[BooksController::class, 'showBooks']);
Route::post('/books-update/{id}',[BooksController::class, 'updateBooks']);
Route::get('deleteBook/{id}',[BooksController::class, 'deleteBook']);
//Admin List of Books End



//Book Category Page Start
Route::get('/admin/book-category',[BookCategoryController::class, 'adminBookCategory']);
Route::post('/addBookCategory',[BookCategoryController::class, 'addBookCategory']);
Route::get('/book-category-update/{id}',[BookCategoryController::class, 'showBookCategory']);
Route::post('/book-category-update/{id}',[BookCategoryController::class, 'updateBookCategory']);
Route::get('deleteBookCategory/{id}',[BookCategoryController::class, 'deleteBookCategory']);
//Book Category Page End



//Admin Electronic Resources Start
Route::get('/admin/electronic-resources',[ElectronicResourcesController::class, 'adminElectronicResources']);
Route::post('/addElectronicResources',[ElectronicResourcesController::class, 'addElectronicResources']);
Route::get('/electronic-resources-update/{id}',[ElectronicResourcesController::class, 'showElectronicResources']);
Route::post('/electronic-resources-update/{id}',[ElectronicResourcesController::class, 'updateElectronicResources']);
Route::get('deleteElectronicResources/{id}',[ElectronicResourcesController::class, 'deleteElectronicResources']);
//Admin Electronic Resources End



//Admin Borrow Books Start
Route::get('/admin/borrow-books',[BorrowBooksController::class, 'adminBorrowBooks']);
Route::get('approveBorrowBook/{id}',[BorrowBooksController::class, 'adminApproveBorrowBooks']);
Route::get('declineBorrowBook/{id}',[BorrowBooksController::class, 'adminDeclineBorrowBooks']);
Route::get('/borrow-books-approve/{id}',[BorrowBooksController::class, 'showBorrowBooksApprove']);
Route::post('/borrow-books-approve/{id}',[BorrowBooksController::class, 'updateBorrowBooksApprove']);
Route::get('/borrow-books-decline/{id}',[BorrowBooksController::class, 'showBorrowBooksDecline']);
Route::post('/borrow-books-decline/{id}',[BorrowBooksController::class, 'updateBorrowBooksDecline']);
//Admin Borrow Books End



//Admin Appointment Page Start
Route::get('/admin/appointment',[AppointmentController::class, 'adminAppointmentPage']);
Route::post('approve/{id}',[AppointmentController::class, 'adminApproveAppointment']);
Route::get('decline/{id}',[AppointmentController::class, 'adminDeclineAppointment']);
Route::get('delete/{id}',[AppointmentController::class, 'destroy']);
Route::get('/appointment-approve/{id}',[AppointmentController::class, 'showAppointmentApprove']);
Route::post('/appointment-approve/{id}',[AppointmentController::class, 'updateAppointmentApprove']);
Route::get('/appointment-decline/{id}',[AppointmentController::class, 'showAppointmentDecline']);
Route::post('/appointment-decline/{id}',[AppointmentController::class, 'updateAppointmentDecline']);
//Admin Appointment Page End



//Manage Website Start
Route::get('/admin/manage-website',[AdminController::class, 'adminManageWebsite']);
//Manage Website End



//About Us Section Start
Route::get('/admin/about-section',[AboutSectionController::class, 'adminAboutSection']);
Route::get('about-section-add',[AboutSectionController::class, 'addAboutSection']);
Route::post('/addAbout',[AboutSectionController::class, 'addAbout']);
Route::post('about-section-add/upload', [AboutSectionController::class, "upload"])->name('ckeditor.image-upload');
Route::get('/about-section-update/{id}',[AboutSectionController::class, 'showAbout']);
Route::post('/about-section-update/{id}',[AboutSectionController::class, 'updateAbout']);
Route::get('deleteAboutSection/{id}',[AboutSectionController::class, 'deleteAboutSection']);
Route::get('showAboutSection/{id}',[AboutSectionController::class, 'showAboutSection']);
Route::get('hideAboutSection/{id}',[AboutSectionController::class, 'hideAboutSection']);
//About Us Section End



//Services Section Start
Route::get('/admin/services-section',[ServicesSectionController::class, 'adminServicesSection']);
Route::get('services-section-add',[ServicesSectionController::class, 'addServicesSection']);
Route::post('/addServices',[ServicesSectionController::class, 'addServices']);
Route::get('/services-section-update/{id}',[ServicesSectionController::class, 'showServices']);
Route::post('/services-section-update/{id}',[ServicesSectionController::class, 'updateServices']);
Route::get('deleteServiceSection/{id}',[ServicesSectionController::class, 'deleteServiceSection']);
Route::get('showServicesSection/{id}',[ServicesSectionController::class, 'showServicesSection']);
Route::get('hideServicesSection/{id}',[ServicesSectionController::class, 'hideServicesSection']);
//Services Section End



//Programs Section Start
Route::get('/admin/programs-section',[ProgramsSectionController::class, 'adminProgramsSection']);
Route::get('programs-section-add',[ProgramsSectionController::class, 'addProgramsSection']);
Route::post('/addPrograms',[ProgramsSectionController::class, 'addPrograms']);
Route::get('/programs-section-update/{id}',[ProgramsSectionController::class, 'showPrograms']);
Route::post('/programs-section-update/{id}',[ProgramsSectionController::class, 'updatePrograms']);
Route::get('deleteProgramSection/{id}',[ProgramsSectionController::class, 'deleteProgramSection']);
Route::get('showProgramsSection/{id}',[ProgramsSectionController::class, 'showProgramsSection']);
Route::get('hideProgramsSection/{id}',[ProgramsSectionController::class, 'hideProgramsSection']);
//Programs Section End



//Awards Section Start
Route::get('/admin/awards-section',[AwardsSectionController::class, 'adminAwardsSection']);
Route::get('awards-section-add',[AwardsSectionController::class, 'addAwardsSection']);
Route::post('/addAwards',[AwardsSectionController::class, 'addAwards']);
Route::get('/awards-section-update/{id}',[AwardsSectionController::class, 'showAwards']);
Route::post('/awards-section-update/{id}',[AwardsSectionController::class, 'updateAwards']);
Route::get('deleteAwardSection/{id}',[AwardsSectionController::class, 'deleteAwardSection']);
Route::get('showAwardsSection/{id}',[AwardsSectionController::class, 'showAwardsSection']);
Route::get('hideAwardsSection/{id}',[AwardsSectionController::class, 'hideAwardsSection']);
//Awards Section End



//Gallery Section Start
Route::get('/admin/gallery-section',[GallerySectionController::class, 'adminGallerySection']);
Route::get('gallery-section-add',[GallerySectionController::class, 'addGallerySection']);
Route::post('/addImage',[GallerySectionController::class, 'addImage']);
Route::get('/gallery-section-update/{id}',[GallerySectionController::class, 'showImage']);
Route::post('/gallery-section-update/{id}',[GallerySectionController::class, 'updateImage']);
Route::get('deleteImage/{id}',[GallerySectionController::class, 'deleteImage']);
Route::get('showGallerySection/{id}',[GallerySectionController::class, 'showGallerySection']);
Route::get('hideGallerySection/{id}',[GallerySectionController::class, 'hideGallerySection']);
//Gallery Section End



//News Section Start
Route::get('/admin/news-section',[NewsSectionController::class, 'adminNewsSection']);
Route::get('news-section-add',[NewsSectionController::class, 'addNewsSection']);
Route::post('/addNews',[NewsSectionController::class, 'addNews']);
Route::get('/news-section-update/{id}',[NewsSectionController::class, 'showNews']);
Route::post('/news-section-update/{id}',[NewsSectionController::class, 'updateNews']);
Route::get('deleteNews/{id}',[NewsSectionController::class, 'deleteNews']);
Route::get('deleteNewsSection/{id}',[NewsSectionController::class, 'deleteNewsSection']);
Route::get('showNewsSection/{id}',[NewsSectionController::class, 'showNewsSection']);
Route::get('hideNewsSection/{id}',[NewsSectionController::class, 'hideNewsSection']);
//News Section End



//Contact Us Form Start
Route::get('/admin/contact-form',[AdminController::class, 'adminContactForm']);
//Contact Us Form End



//Clients Start
Route::get('/admin/clients',[AdminController::class, 'adminClients']);
Route::get('/change-role-user/{id}',[AdminController::class, 'showChangeRoleUser']);
Route::post('/change-role-user/{id}',[AdminController::class, 'updateChangeRoleUser']);
Route::get('/change-role-admin/{id}',[AdminController::class, 'showChangeRoleAdmin']);
Route::post('/change-role-admin/{id}',[AdminController::class, 'updateChangeRoleAdmin']);
//Clients End



//FCK Editor Start
Route::get('fckeditor', [CKEditorController::class, "fckeditor"]);
Route::post('fckeditor/upload', [CKEditorController::class, "upload"])->name('ckeditor.image-upload');
Route::post('fckeditorStore', [CKEditorController::class, "ckeditorStore"]);
//FCK Editor End



Route::get('/admin/list-category',[AdminController::class, 'listCategory']);
Route::get('/list-books/{id}',[AdminController::class, 'listBooks']);

Route::get('studentCreate',[App\Http\Controllers\StudentController::class, 'studentCreate']);
Route::post('students', [App\Http\Controllers\StudentController::class, 'store']);

Route::post('awards_section', [App\Http\Controllers\AwardsSectionController::class, 'store']);
Route::post('gallery_section', [GallerySectionController::class, 'store']);
Route::post('news_section', [NewsSectionController::class, 'store']);
Route::post('books', [BooksController::class, 'store']);
Route::post('electronic_resources', [ElectronicResourcesController::class, 'store']);