<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Books;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{
    public function adminBooks() {
        //$books = DB::select('SELECT * FROM books left join book_category on book_category.id=books.category_id');
        //$category = DB::select('SELECT * FROM book_category');
        //return view('admin/books',['books'=>$books,'category'=>$category]);
        $books = DB::select('SELECT * FROM books');
        return view('admin/books',['books'=>$books]);
    }

    public function addBook(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'title'=>'required',
            'book_category'=>'required',
            'author'=>'required',
            'copyright'=>'required',
            'publisher'=>'required',
            'total'=>'required',
            'image'=>'required',
        ]);

        $title = $request->input('title');
        $book_category = $request->input('book_category');
        $author = $request->input('author');
        $copyright = $request->input('copyright');
        $publisher = $request->input('publisher');
        $total = $request->input('total');
        $image = $request->input('image');

        DB::insert('insert into books (title,book_category,author,copyright,publisher,total,image) 
                    values(?,?,?,?,?,?,?)',[$title,$book_category,$author,$copyright,$publisher,$total,$image]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/books')->with('insert_message','Added Book');
    }

    public function showBooks($id) {
        $books = DB::select('SELECT * FROM books where id = ?',[$id]);
        return view('admin/books-update',['books'=>$books]);
    }

    public function updateBooks(Request $request,$id) {
        $title = $request->input('title');
        $book_category = $request->input('book_category');
        $author = $request->input('author');
        $publisher = $request->input('publisher');
        $copyright = $request->input('copyright');
        $total = $request->input('total');

        DB::update('update books set title = ?, book_category = ?, author = ?, publisher = ?, copyright = ?, total = ?
                    where id = ?',[$title,$book_category,$author,$publisher,$copyright,$total,$id]);
        return redirect('admin/books');
    }

    public function deleteBook($id) {
        DB::delete('delete from books where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/books')->with('delete_message','Deleted Book Category');
    }

    public function store(Request $request)
    {
        $books = new Books;
        $books->title = $request->input('title');
        $books->book_category = $request->input('book_category');
        $books->author = $request->input('author');
        $books->publisher = $request->input('publisher');
        $books->copyright = $request->input('copyright');
        $books->total = $request->input('total');

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/books/', $filename);
            $books->image = $filename;
        }

        $books->save();
        return redirect('admin/books');
    }
}
