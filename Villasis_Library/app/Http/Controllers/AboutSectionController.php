<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutSectionController extends Controller
{
    public function adminAboutSection() {
        $about_section = DB::select('SELECT * FROM about_section');
        return view('admin/about-section',['about_section'=>$about_section]);
    }

    public function addAboutSection() {
        return view('admin/about-section-add');
    }

    public function addAbout(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'name'=>'required',
            'content'=>'required',
        ]);

        $name = $request->input('name');
        $content = $request->input('content');
        $status = $request->input('status');

        DB::insert('insert into about_section (name,content,status) 
                    values(?,?,?)',[$name,$content,$status]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/about-section')->with('insert_message','Added About Us Content');
    }

    public function upload(Request $request)
    {
        if($request->hasFile('upload')) 
        {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $msg = 'Image successfully uploaded';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function showAbout($id) {
        $about_section = DB::select('SELECT * FROM about_section where id = ?',[$id]);
        return view('admin/about-section-update',['about_section'=>$about_section]);
    }

    public function updateAbout(Request $request,$id) {
        $name = $request->input('name');
        $content = $request->input('content');
        DB::update('update about_section set name = ?, content = ? where id = ?',[$name,$content,$id]);
        return redirect('admin/about-section');
    }

    public function deleteAboutSection($id) {
        DB::delete('delete from about_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/about-section')->with('delete_message','Deleted About Us Section content');
    }

    public function showAboutSection($id) {
        $about_section = DB::select('select * from about_section where id = ?',[$id]);
        DB::update('update about_section set status = 1 where id = ?',[$id]);
        return redirect('admin/about-section');
    }

    public function hideAboutSection($id) {
        $about_section = DB::select('select * from about_section where id = ?',[$id]);
        DB::update('update about_section set status = 0 where id = ?',[$id]);
        return redirect('admin/about-section');
    }

}