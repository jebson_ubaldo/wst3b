<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{
    public function adminAppointmentPage() {
        $appointments = DB::select('SELECT * FROM appointments');
        return view('admin/appointment',['appointments'=>$appointments]);
    }

    public function adminApproveAppointment($id) {
        $appointments = DB::select('select * from appointments where id = ?',[$id]);
        DB::update('update appointments set status = 1 where id = ?',[$id]);
        return redirect('admin/appointment');
    }

    public function adminDeclineAppointment($id) {
        $appointments = DB::select('select * from appointments where id = ?',[$id]);
        DB::update('update appointments set status = 2 where id = ?',[$id]);
        return redirect('admin/appointment');
    }

    public function destroy($id) {
        DB::delete('delete from appointments where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        echo '<a href = "/admin/appointment">Click Here</a> to go back.';
    }

    public function showAppointmentApprove($id) {
        $appointment = DB::select('SELECT * FROM appointments where id = ?',[$id]);
        return view('admin/appointment-approve',['appointment'=>$appointment]);
    }

    public function updateAppointmentApprove(Request $request,$id) {
        $name = $request->input('name');
        $service_availed = $request->input('service_availed');
        $appointment_date = $request->input('appointment_date');
        $appointment_time = $request->input('appointment_time');
        $status = $request->input('status');

        DB::update('update appointments set name = ?, service_availed = ?, appointment_date = ?, appointment_time = ?, status = ?
                    where id = ?',[$name,$service_availed,$appointment_date,$appointment_time,$status,$id]);
        return redirect('admin/appointment');
    }
}