<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function AppointmentPage($id) {
        $appointments = DB::select("SELECT * FROM appointments WHERE user_id='$id'");
        return view('appointment',['appointments'=>$appointments]);
    }

    public function insert(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'service_availed'=>'required',
            'appointment_date'=>'required|date|after:today',
            'appointment_time'=>'required',
        ]);

        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $service_availed = $request->input('service_availed');
        $appointment_date = $request->input('appointment_date');
        $appointment_time = $request->input('appointment_time');
        $status = $request->input('status');
        $accepted = $request->input('accepted');


        DB::insert('insert into appointments (user_id,name,email,service_availed,appointment_date,appointment_time,status,accepted) 
                    values(?,?,?,?,?,?,?,?)',[$user_id,$name,$email,$service_availed,$appointment_date,$appointment_time,$status,$accepted]);
        
        return redirect('/appointment-successful');
    }

    public function showUserAppointment($id) {
        $appointments = DB::select('select * from appointments where id = ?',[$id]);
        return view('appointment-update',['appointments'=>$appointments]);
    }

    public function editUserAppointment(Request $request,$id) {
        $service_availed = $request->input('service_availed');
        $appointment_date = $request->input('appointment_date');
        $appointment_time = $request->input('appointment_time');
        DB::update('UPDATE appointments SET service_availed = ?, appointment_date = ?, appointment_time = ? WHERE id = ?',
                    [$service_availed,$appointment_date,$appointment_time,$id]);
                    
        return redirect('/home');
    }

    public function userAppointmentSuccessful() {
        return view('/appointment-successful');
    }

    public function userAppointmentCancel() {
        return view('/appointment-cancel');
    }
  

    public function cancelUserAppointment($id) {
        DB::delete('DELETE FROM appointments WHERE id = ?',[$id]);
        return redirect('/appointment-cancel');
    }

    public function userBookCategory() {
        $category = DB::SELECT('SELECT * FROM book_category');
        return view('book-category',['category'=>$category]);
    }

    //public function userBooks($id) {
    //    $books = DB::select("SELECT * FROM books left join book_category on book_category.id=books.category_id WHERE category_id='$id'");
    //    $category = DB::select('SELECT * FROM book_category');
    //    return view('/books',['books'=>$books]);
    //}

    public function userBooks() {
        $books = DB::SELECT('SELECT * FROM books');
        return view('/books',['books'=>$books]);
    }

    public function userBorrowBooks($id) {
        $borrow = DB::SELECT("SELECT * FROM borrow_books WHERE user_id='$id'");
        return view('/borrow-books',['borrow'=>$borrow]);
    }

    public function borrowBook(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'title'=>'required',
            'author'=>'required',
            'copyright'=>'required',
            'publisher'=>'required',
        ]);

        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $title = $request->input('title');
        $author = $request->input('author');
        $copyright = $request->input('copyright');
        $publisher = $request->input('publisher');
        $borrow_date = $request->input('borrow_date');
        $return_date = $request->input('return_date');
        $status = $request->input('status');
        $remarks = $request->input('remarks');

        DB::insert('insert into borrow_books (user_id,name,email,title,author,copyright,publisher,borrow_date,return_date,status,remarks) 
                    values(?,?,?,?,?,?,?,?,?,?,?)',[$user_id,$name,$email,$title,$author,$copyright,$publisher,$borrow_date,$return_date,$status,$remarks]);

        return redirect('/borrow-book-successful');
    }

    public function userBorrowBookSuccessful() {
        return view('/borrow-book-successful');
    }

    public function userBorrowBookCancel() {
        return view('/borrow-book-cancel');
    }

    public function cancelBorrowBook($id) {
        DB::delete('DELETE FROM borrow_books where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('/borrow-book-cancel')->with('delete_message','Deleted News Section content');
    }

    public function userElectronicResources() {
        $electronic_resources = DB::select('SELECT * FROM electronic_resources');
        return view('electronic-resources',['electronic_resources'=>$electronic_resources]);
    }
}