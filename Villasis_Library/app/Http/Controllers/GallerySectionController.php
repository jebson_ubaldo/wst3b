<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GallerySectionController extends Controller
{
    public function adminGallerySection() {
        $gallery_section = DB::select('SELECT * FROM gallery_section');
        return view('admin/gallery-section',['gallery_section'=>$gallery_section]);
    }

    public function addGallerySection() {
        return view('admin/gallery-section-add');
    }

    public function addImage(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'details'=>'required',
            'image' => 'required',
        ]);

        $details = $request->input('details');
        $image = $request->input('image');
        $status = $request->input('status');

        DB::insert('insert into gallery_section (details,image,status) 
                    values(?,?,?)',[$details,$image,$status]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/gallery-section')->with('insert_message','Added Image');
    }

    public function showImage($id) {
        $gallery_section = DB::select('SELECT * FROM gallery_section where id = ?',[$id]);
        return view('admin/gallery-section-update',['gallery_section'=>$gallery_section]);
    }

    public function updateImage(Request $request,$id) {
        $details = $request->input('details');
        DB::update('update gallery_section set details = ? where id = ?',[$details,$id]);
        return redirect('admin/gallery-section');
    }

    public function deleteImage($id) {
        DB::delete('delete from gallery_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/gallery-section')->with('delete_message','Deleted Image');
    }

    public function showGallerySection($id) {
        $gallery_section = DB::select('select * from gallery_section where id = ?',[$id]);
        DB::update('update gallery_section set status = 1 where id = ?',[$id]);
        return redirect('admin/gallery-section');
    }

    public function hideGallerySection($id) {
        $gallery_section = DB::select('select * from gallery_section where id = ?',[$id]);
        DB::update('update gallery_section set status = 0 where id = ?',[$id]);
        return redirect('admin/gallery-section');
    }

    public function store(Request $request)
    {
        $gallery = new Gallery;
        $gallery->details = $request->input('details');
        $gallery->status = $request->input('status');

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/gallery/', $filename);
            $gallery->image = $filename;
        }

        $gallery->save();
        return redirect('admin/gallery-section');
    }
}
