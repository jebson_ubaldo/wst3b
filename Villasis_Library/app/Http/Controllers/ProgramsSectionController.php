<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProgramsSectionController extends Controller
{
    public function adminProgramsSection() {
        $programs_section = DB::select('SELECT * FROM programs_section');
        return view('admin/programs-section',['programs_section'=>$programs_section]);
    }

    public function addProgramsSection() {
        return view('admin/programs-section-add');
    }

    public function addPrograms(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'name'=>'required',
        ]);

        $name = $request->input('name');
        $status = $request->input('status');

        DB::insert('insert into programs_section (name,status) 
                    values(?,?)',[$name,$status]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/programs-section')->with('insert_message','Added Program');
    }

    public function showPrograms($id) {
        $programs_section = DB::select('SELECT * FROM programs_section where id = ?',[$id]);
        return view('admin/programs-section-update',['programs_section'=>$programs_section]);
    }

    public function updatePrograms(Request $request,$id) {
        $name = $request->input('name');
        DB::update('update programs_section set name = ? where id = ?',[$name,$id]);
        return redirect('admin/programs-section');
    }

    public function deleteProgramSection($id) {
        DB::delete('delete from programs_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/programs-section')->with('delete_message','Deleted Program Section content');
    }

    public function showProgramsSection($id) {
        $programs_section = DB::select('select * from programs_section where id = ?',[$id]);
        DB::update('update programs_section set status = 1 where id = ?',[$id]);
        return redirect('admin/programs-section');
    }

    public function hideProgramsSection($id) {
        $programs_section = DB::select('select * from programs_section where id = ?',[$id]);
        DB::update('update programs_section set status = 0 where id = ?',[$id]);
        return redirect('admin/programs-section');
    }

}