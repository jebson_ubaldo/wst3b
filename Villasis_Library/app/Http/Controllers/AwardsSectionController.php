<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Award;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AwardsSectionController extends Controller
{
    public function adminAwardsSection() {
        $awards_section = DB::select('SELECT * FROM awards_section');
        return view('admin/awards-section',['awards_section'=>$awards_section]);
    }

    public function addAwardsSection() {
        return view('admin/awards-section-add');
    }

    public function addAwards(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'details'=>'required',
            'image' => 'required',
        ]);

        $details = $request->input('details');
        $image = $request->file('image');
        $status = $request->input('status');

        DB::insert('insert into awards_section (details,image,status) 
                    values(?,?,?)',[$details,$image,$status]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/awards-section')->with('insert_message','Added Award');
    }

    public function showAwards($id) {
        $awards_section = DB::select('SELECT * FROM awards_section where id = ?',[$id]);
        return view('admin/awards-section-update',['awards_section'=>$awards_section]);
    }

    public function updateAwards(Request $request,$id) {
        $details = $request->input('details');
        DB::update('update awards_section set details = ? where id = ?',[$details,$id]);
        return redirect('admin/awards-section');
    }

    public function deleteAwardSection($id) {
        DB::delete('delete from awards_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/awards-section')->with('delete_message','Deleted Award Section content');
    }

    public function showAwardsSection($id) {
        $awards_section = DB::select('select * from awards_section where id = ?',[$id]);
        DB::update('update awards_section set status = 1 where id = ?',[$id]);
        return redirect('admin/awards-section');
    }

    public function hideAwardsSection($id) {
        $awards_section = DB::select('select * from awards_section where id = ?',[$id]);
        DB::update('update awards_section set status = 0 where id = ?',[$id]);
        return redirect('admin/awards-section');
    }


    public function store(Request $request)
    {
        $award = new Award;
        $award->details = $request->input('details');
        $award->status = $request->input('status');

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/awards/', $filename);
            $award->image = $filename;
        }

        $award->save();
        return redirect('admin/awards-section');
    }
}
