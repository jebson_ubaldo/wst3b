<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function adminManageWebsite() {
        return view('admin/manage-website');
    }

    public function adminContactForm() {
        $contact_form = DB::select('SELECT * FROM contact_form');
        return view('admin/contact-form',['contact_form'=>$contact_form]);
    }

    public function adminClients() {
        //$users = DB::select("SELECT * FROM users where is_admin='0'");
        $users = DB::select("SELECT * FROM users where id != '1'");
        return view('admin/clients',['users'=>$users]);
    }

    public function showChangeRoleUser($id) {
        $users = DB::select('SELECT * FROM users where id = ?',[$id]);
        return view('admin/change-role-user',['users'=>$users]);
    }

    public function updateChangeRoleUser(Request $request,$id) {
        $is_admin = $request->input('is_admin');

        DB::update('update users set is_admin = ?
                    where id = ?',[$is_admin,$id]);
        return redirect('admin/clients');
    }

    public function showChangeRoleAdmin($id) {
        $users = DB::select('SELECT * FROM users where id = ?',[$id]);
        return view('admin/change-role-admin',['users'=>$users]);
    }

    public function updateChangeRoleAdmin(Request $request,$id) {
        $is_admin = $request->input('is_admin');

        DB::update('update users set is_admin = ?
                    where id = ?',[$is_admin,$id]);
        return redirect('admin/clients');
    }

    public function listCategory() {
        $category = DB::select('SELECT * FROM book_category');
        return view('admin/list-category',['category'=>$category]);
    }

    public function listBooks($id) {
        $books = DB::select("SELECT * FROM books left join book_category on book_category.id=books.category_id WHERE category_id='$id'");
        $category = DB::select('SELECT * FROM book_category');
        return view('admin/list-books',['books'=>$books]);
    }
}