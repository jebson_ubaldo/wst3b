<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BorrowBooksController extends Controller
{
    public function adminBorrowBooks() {
        $borrow = DB::select('SELECT * FROM borrow_books');
        return view('admin/borrow-books',['borrow'=>$borrow]);
    }

    public function adminApproveBorrowBooks($id) {
        $borrow = DB::select('select * from borrow_books where id = ?',[$id]);
        DB::update('update borrow_books set status = 1 where id = ?',[$id]);
        return redirect('admin/borrow-books');
    }

    public function adminDeclineBorrowBooks($id) {
        $borrow = DB::select('select * from borrow_books where id = ?',[$id]);
        DB::update('update borrow_books set status = 2 where id = ?',[$id]);
        return redirect('admin/borrow-books');
    }

    public function showBorrowBooksApprove($id) {
        $borrow = DB::select('SELECT * FROM borrow_books where id = ?',[$id]);
        return view('admin/borrow-books-approve',['borrow'=>$borrow]);
    }

    public function updateBorrowBooksApprove(Request $request,$id) {
        $name = $request->input('name');
        $title = $request->input('title');
        $borrow_date = $request->input('borrow_date');
        $return_date = $request->input('return_date');
        $remarks = $request->input('remarks');
        $status = $request->input('status');

        DB::update('update borrow_books set name = ?, title = ?, borrow_date = ?, return_date = ?, remarks = ?, status = ?
                    where id = ?',[$name,$title,$borrow_date,$return_date,$remarks,$status,$id]);
        return redirect('admin/borrow-books');
    }

    public function showBorrowBooksDecline($id) {
        $borrow = DB::select('SELECT * FROM borrow_books where id = ?',[$id]);
        return view('admin/borrow-books-decline',['borrow'=>$borrow]);
    }

    public function updateBorrowBooksDecline(Request $request,$id) {
        $name = $request->input('name');
        $title = $request->input('title');
        $borrow_date = $request->input('borrow_date');
        $return_date = $request->input('return_date');
        $remarks = $request->input('remarks');
        $status = $request->input('status');

        DB::update('update borrow_books set name = ?, title = ?, borrow_date = ?, return_date = ?, remarks = ?, status = ?
                    where id = ?',[$name,$title,$borrow_date,$return_date,$remarks,$status,$id]);
        return redirect('admin/borrow-books');
    }
}
