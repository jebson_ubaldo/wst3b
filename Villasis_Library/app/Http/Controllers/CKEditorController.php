<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ckeditor;

class CKEditorController extends Controller
{

    public function fckeditor(){
        return view('fckeditor');
    }




public function upload(Request $request)
{
if($request->hasFile('upload')) {
$originName = $request->file('upload')->getClientOriginalName();
$fileName = pathinfo($originName, PATHINFO_FILENAME);
$extension = $request->file('upload')->getClientOriginalExtension();
$fileName = $fileName.'_'.time().'.'.$extension;
$request->file('upload')->move(public_path('images'), $fileName);
$CKEditorFuncNum = $request->input('CKEditorFuncNum');
$url = asset('images/'.$fileName);
$msg = 'Image successfully uploaded';
$response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
@header('Content-type: text/html; charset=utf-8');
echo $response;
}
}

 public function ckeditorStore(Request $request)
{
//ini_set('memory_limit', '2048M');
$post = [ "name" => $request->name,
"content" => $request->content
];

 $post = Ckeditor::create($post);
return back()->with("success", "Post has been created");
}
}