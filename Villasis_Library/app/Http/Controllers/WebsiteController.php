<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WebsiteController extends Controller
{
    public function submitContactForm(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'fullname'=>'required',
            'email'=>'required',
            'subject'=>'required',
            'message'=>'required',
        ]);

        $fullname = $request->input('fullname');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');
        $message_date = $request->input('message_date');
        $message_time = $request->input('message_time');

        DB::insert('insert into contact_form (fullname,email,subject,message,message_date,message_time) 
                    values(?,?,?,?,?,?)',[$fullname,$email,$subject,$message,$message_date,$message_time]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('/#contact')->with('insert_message','Submitted Message');
    }

    public function manageWebsite() {
        $about_section = DB::select('SELECT * FROM about_section');
        $services_section = DB::select('SELECT * FROM services_section');
        $programs_section = DB::select('SELECT * FROM programs_section');
        $awards_section = DB::select('SELECT * FROM awards_section');
        $gallery_section = DB::select('SELECT * FROM gallery_section');
        $news_section = DB::select('SELECT * FROM news_section');

        return view('index',['about_section'=>$about_section,'services_section'=>$services_section,'programs_section'=>$programs_section,
                    'awards_section'=>$awards_section,'gallery_section'=>$gallery_section,'news_section'=>$news_section]);
    }

    public function scanQrCode() {
        return view('qr-code');
    }
}
