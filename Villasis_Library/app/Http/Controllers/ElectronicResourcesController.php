<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ElectronicResources;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ElectronicResourcesController extends Controller
{
    public function adminElectronicResources() {
        $electronic_resources = DB::select('SELECT * FROM electronic_resources');
        return view('admin/electronic-resources',['electronic_resources'=>$electronic_resources]);
    }

    public function addElectronicResources(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'name'=>'required',
            'link'=>'required',
        ]);

        $name = $request->input('name');
        $link = $request->input('link');

        DB::insert('insert into electronic_resources (name,link) 
                    values(?,?)',[$name,$link]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/electronic-resources')->with('insert_message','Added Electronic Resources');
    }

    public function showElectronicResources($id) {
        $electronic_resources = DB::select('SELECT * FROM electronic_resources where id = ?',[$id]);
        return view('admin/electronic-resources-update',['electronic_resources'=>$electronic_resources]);
    }

    public function updateElectronicResources(Request $request,$id) {
        $name = $request->input('name');
        $link = $request->input('link');
        DB::update('update electronic_resources set name = ?, link = ? where id = ?',[$name,$link,$id]);
        return redirect('admin/electronic-resources');
    }

    public function deleteElectronicResources($id) {
        DB::delete('delete from electronic_resources where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/electronic-resources')->with('delete_message','Deleted Electronic Resource');
    }

    public function store(Request $request)
    {
        $electronic_resources = new ElectronicResources;
        $electronic_resources->name = $request->input('name');
        $electronic_resources->link = $request->input('link');

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/electronic-resources/', $filename);
            $electronic_resources->image = $filename;
        }

        $electronic_resources->save();
        return redirect('admin/electronic-resources');
    }
}