<?php

namespace App\Http\Controllers;

use App\Models\Student;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function studentCreate(){
        return view ('studentCreate');
    }

    public function store(Request $request)
    {
        $student = new Student;
        $student->name = $request->input('name');

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/students/', $filename);
            $student->image = $filename;
        }

        $student->save();
        return redirect()->back()->with('message','Student Image Upload Successfully');
    }
}