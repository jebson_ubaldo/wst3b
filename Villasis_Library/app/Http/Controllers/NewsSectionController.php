<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsSectionController extends Controller
{
    public function adminNewsSection() {
        $news_section = DB::select('SELECT * FROM news_section');
        return view('admin/news-section',['news_section'=>$news_section]);
    }

    public function addNewsSection() {
        return view('admin/news-section-add');
    }

    public function addNews(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'news_headline'=>'required',
            'news_content'=>'required',
            'image' => 'required',
        ]);

        $news_headline = $request->input('news_headline');
        $news_content = $request->input('news_content');
        $image = $request->input('image');
        $status = $request->input('status');

        DB::insert('insert into news_section (news_headline,news_content,image,status) 
                    values(?,?,?,?)',[$news_headline,$news_content,$image,$status]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/news-section')->with('insert_message','Added News');
    }

    public function showNewsSection($id) {
        $news_section = DB::select('select * from news_section where id = ?',[$id]);
        DB::update('update news_section set status = 1 where id = ?',[$id]);
        return redirect('admin/news-section');
    }

    public function hideNewsSection($id) {
        $news_section = DB::select('select * from news_section where id = ?',[$id]);
        DB::update('update news_section set status = 0 where id = ?',[$id]);
        return redirect('admin/news-section');
    }

    public function showNews($id) {
        $news_section = DB::select('SELECT * FROM news_section where id = ?',[$id]);
        return view('admin/news-section-update',['news_section'=>$news_section]);
    }

    public function updateNews(Request $request,$id) {
        $news_headline = $request->input('news_headline');
        $news_content = $request->input('news_content');
        DB::update('update news_section set news_headline = ?, news_content = ? where id = ?',[$news_headline,$news_content,$id]);
        return redirect('admin/news-section');
    }

    public function deleteNews($id) {
        DB::delete('delete from news_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/news-section')->with('delete_message','Deleted News');
    }

    public function deleteNewsSection($id) {
        DB::delete('delete from news_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/news-section')->with('delete_message','Deleted News Section content');
    }

    public function store(Request $request)
    {
        $news = new News;
        $news->news_headline = $request->input('news_headline');
        $news->news_content = $request->input('news_content');
        $news->status = $request->input('status');

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/news/', $filename);
            $news->image = $filename;
        }

        $news->save();
        return redirect('admin/news-section');
    }
}