<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServicesSectionController extends Controller
{
    public function adminServicesSection() {
        $services_section = DB::select('SELECT * FROM services_section');
        return view('admin/services-section',['services_section'=>$services_section]);
    }

    public function addServicesSection() {
        return view('admin/services-section-add');
    }

    public function addServices(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'name'=>'required',
            'details'=>'required',
            'icon'=>'required',
        ]);

        $name = $request->input('name');
        $details = $request->input('details');
        $icon = $request->input('icon');
        $status = $request->input('status');

        DB::insert('insert into services_section (name,details,icon,status) 
                    values(?,?,?,?)',[$name,$details,$icon,$status]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/services-section')->with('insert_message','Added Service');
    }

    public function showServices($id) {
        $services_section = DB::select('SELECT * FROM services_section where id = ?',[$id]);
        return view('admin/services-section-update',['services_section'=>$services_section]);
    }

    public function updateServices(Request $request,$id) {
        $name = $request->input('name');
        $details = $request->input('details');
        $icon = $request->input('icon');
        DB::update('update services_section set name = ?, details = ?, icon = ? where id = ?',[$name,$details,$icon,$id]);
        return redirect('admin/services-section');
    }

    public function deleteServiceSection($id) {
        DB::delete('delete from services_section where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/services-section')->with('delete_message','Deleted Service Section content');
    }

    public function showServicesSection($id) {
        $services_section = DB::select('select * from services_section where id = ?',[$id]);
        DB::update('update services_section set status = 1 where id = ?',[$id]);
        return redirect('admin/services-section');
    }

    public function hideServicesSection($id) {
        $services_section = DB::select('select * from services_section where id = ?',[$id]);
        DB::update('update services_section set status = 0 where id = ?',[$id]);
        return redirect('admin/services-section');
    }

}