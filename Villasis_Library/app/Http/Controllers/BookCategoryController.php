<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BookCategoryController extends Controller
{
    public function adminBookCategory() {
        $book_category = DB::select('SELECT * FROM book_category');
        return view('admin/book-category',['book_category'=>$book_category]);
    }

    public function addBookCategory(Request $request) {
        $input = $request->all();
        $this->validate($request,[
            'category_name'=>'required',
        ]);

        $category_name = $request->input('category_name');

        DB::insert('insert into book_category (category_name) 
                    values(?)',[$category_name]);
        
        echo "Record inserted successfully.<br/>";
        return redirect('admin/book-category')->with('insert_message','Added Book Category');
    }

    public function showBookCategory($id) {
        $book_category = DB::select('SELECT * FROM book_category where id = ?',[$id]);
        return view('admin/book-category-update',['book_category'=>$book_category]);
    }

    public function updateBookCategory(Request $request,$id) {
        $category_name = $request->input('category_name');
        DB::update('update book_category set category_name = ? where id = ?',[$category_name,$id]);
        return redirect('admin/book-category');
    }

    public function deleteBookCategory($id) {
        DB::delete('delete from book_category where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        return redirect('admin/book-category')->with('delete_message','Deleted Book Category');
    }

}
