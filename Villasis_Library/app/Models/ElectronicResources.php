<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectronicResources extends Model
{
    use HasFactory;

    protected $table = 'electronic_resources';

    protected $fillable = [
        'name',
        'link',
        'image',
    ];
}
