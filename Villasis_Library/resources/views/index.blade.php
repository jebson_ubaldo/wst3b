<!DOCTYPE html>
<html lang="en">
<head>
   <!-- Meta Tags -->
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <!-- CSS -->
   <link rel="stylesheet" href="css/style.css">

   <!-- Bootstrap CSS -->
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

   <!-- Font -->
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

   <!-- Icons -->
   <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <!-- Animation -->
   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

   <title>Villasis Municipal Library</title>
   <link rel="icon" type="image/png" href="assets/villasis_library_logo.png">
</head>
<body>
   <!-- Navigation Bar Start -->
   <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white">
      <div class="container">
        <a class="navbar-brand" href="#">
            <img src="assets/villasis_library_logo.png" 
                 alt="villasis_library_logo" 
                 class="img-fluid" 
                 height="50px" width="55px" style="margin-right: 7px;"/>          
            <b style="font-family: 'Nunito', sans-serif;">Villasis Municipal Library</b>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto">

            <li class="nav-item">
              <a class="nav-link" href="#home">Home</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#about">About Us</a>
            </li>
            <!--<li>
               <a class="nav-link" href="#organization">Organization</a>
            </li>-->
            <li class="nav-item">
               <a class="nav-link" href="#services">Services</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#programs">Programs</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#awards">Awards</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#gallery">Gallery</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#news">News</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#contact">Contact</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
          </ul>
        </div>
      </div>
   </nav>
   <!-- Navigation Bar End -->
   

   <!-- Home Section Start -->
   <section id="home">
      <div class="container text-center">
         <div class="row justify-content-center">
            <div class="col-md-12 mt-4">
               <!--<img src="assets/villasis_logo.png" 
                  alt="villasis_logo" 
                  class="img-fluid me-4" 
                  style="max-height: 150px;"/>
               <img src="assets/villasis_library_logo.png" 
                  alt="villasis_logo" 
                  class="img-fluid me-4" 
                  style="max-height: 150px;"/>-->
               <h1 class="text-white display-4 mt-4">Villasis Municipal Library</h1>
               <p class="text-white">We encourage you to READ. Read, Explore and Discover.</p>
               <a href="{{ route('login') }}" class="button mt-4">GET STARTED <i class='bx bx-right-arrow-alt'></i></a>
            </div>
         </div>
      </div>
   </section>
   <!-- Home Section End -->


   <!-- About Us Section Start -->
   <section id="about">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>About Us</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row gy-4">
            @foreach ($about_section as $about_section)
               @if($about_section->status == 0)
                  <div class="col-lg-6 col-sm-6 mb-4 programs d-flex" data-aos="zoom-in">
                     <div class="icon-box me-3">
                        <i class="bx bx-check"></i>
                     </div>
                     <div>
                        <h5 class="title-sm">{{ $about_section->name }}</h5>
                        <p>{{ $about_section->content }}</p>
                     </div>
                  </div>
               @endif 
            @endforeach
         </div>
      </div>
   </section>
   <!-- About Us Section End -->


   <!-- Organization Section Start -->
   <!--<section id="organization">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro">
               <h1>Organization</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-xl-2 col-lg-4 col-md-4 col-8 m-4">
               <div class="rm-rounded-5 card border-light rm-box-shadow">
                  <img src="assets/Offcials/Imee Marcos.jpg" class="img-fluid mc-avatar card-img-top p-4 d-block mx-auto" alt="organization_2">
                  <div class="card-body">
                     <h3 class="rm-display-4 rm-text-primary text-center m-0">María Imelda Marcos</h3>
                     <p class="card-text text-center rm-text-muted">Municipal Vice Mayor</p>
                  </div>
              </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-4 col-8 m-4">
               <div class="rm-rounded-5 card border-light rm-box-shadow">
                  <img src="assets/Offcials/Imee Marcos.jpg" class="img-fluid mc-avatar card-img-top p-4 d-block mx-auto" alt="organization_2">
                  <div class="card-body">
                     <h3 class="rm-display-4 rm-text-primary text-center m-0">María Imelda Marcos</h3>
                     <p class="card-text text-center rm-text-muted">Municipal Vice Mayor</p>
                  </div>
              </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-4 col-8 m-4">
               <div class="rm-rounded-5 card border-light rm-box-shadow">
                  <img src="assets/Offcials/Imee Marcos.jpg" class="img-fluid mc-avatar card-img-top p-4 d-block mx-auto" alt="organization_2">
                  <div class="card-body">
                     <h3 class="rm-display-4 rm-text-primary text-center m-0">María Imelda Marcos</h3>
                     <p class="card-text text-center rm-text-muted">Municipal Vice Mayor</p>
                  </div>
              </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-4 col-8 m-4">
               <div class="rm-rounded-5 card border-light rm-box-shadow">
                  <img src="assets/Offcials/Imee Marcos.jpg" class="img-fluid mc-avatar card-img-top p-4 d-block mx-auto" alt="organization_2">
                  <div class="card-body">
                     <h3 class="rm-display-4 rm-text-primary text-center m-0">María Imelda Marcos</h3>
                     <p class="card-text text-center rm-text-muted">Municipal Vice Mayor</p>
                  </div>
              </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-4 col-8 m-4">
               <div class="rm-rounded-5 card border-light rm-box-shadow">
                  <img src="assets/Offcials/Imee Marcos.jpg" class="img-fluid mc-avatar card-img-top p-4 d-block mx-auto" alt="organization_2">
                  <div class="card-body">
                     <h3 class="rm-display-4 rm-text-primary text-center m-0">María Imelda Marcos</h3>
                     <p class="card-text text-center rm-text-muted">Municipal Vice Mayor</p>
                  </div>
              </div>
            </div>
         </div>
      </div>
   </section>-->
   <!-- Organization Section End -->


   <!-- Services Section Start -->
   <section id="services">
      <div class="container text-center text-md-start">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>Our Services</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row">
            @foreach ($services_section as $services_section)
               @if($services_section->status == 0)
                  <div class="col-lg-3 col-sm-6 p-4" data-aos="zoom-in">
                     <div class="icon-box">
                        <i class='bx bx-{{ $services_section->icon }}'></i>
                     </div>
                     <h4 class="title-sm mt-4">{{ $services_section->name }}</h4>
                     <p>{{ $services_section->details }}</p>
                  </div>
               @endif
            @endforeach
         </div>
      </div>
   </section>
   <!-- Services Section End -->


   <!-- Programs Section Start -->
   <section id="programs">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>Our Programs</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row gy-4 mt-2">
            @foreach ($programs_section as $programs_section)
               @if($programs_section->status == 0)
                  <div class="col-lg-4 col-sm-6 mb-4 programs d-flex" data-aos="zoom-in">
                     <div class="icon-box me-3">
                        <i class="bx bx-check"></i>
                     </div>
                     <div>
                        <h5 class="title-sm">{{ $programs_section->name }}</h5>
                     </div>
                  </div>
               @endif
            @endforeach
         </div>
      </div>
   </section>
   <!-- Programs Section End -->


   <!-- Awards Section Start -->
   <section id="awards">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>Awards and Recognitions</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row">
            @foreach ($awards_section as $awards_section)
               @if($awards_section->status == 0)
                  <div class="col-md-6 col-lg-4 mb-4" data-aos="zoom-in">
                     <div class="award-1 h-100">
                        <a href="../uploads/awards/{{ $awards_section->image }}">
                           <img src="../uploads/awards/{{ $awards_section->image }}" class="w-100 img-thumbnail" alt="award_section">
                        </a>
                     </div>
                  </div>
               @endif
            @endforeach
         </div>
      </div>
   </section>
   <!-- Awards Section End -->


   <!-- Gallery Section Start -->
   <section id="gallery">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>Gallery</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row"> 
            @foreach ($gallery_section as $gallery_section)
               @if($gallery_section->status == 0)
                  <div class="col-md-6 col-lg-4 mb-4" data-aos="zoom-in">
                     <div class="award-1 h-100">
                        <a href="../uploads/gallery/{{ $gallery_section->image }}">
                           <img src="../uploads/gallery/{{ $gallery_section->image }}" class="w-100 img-thumbnail" alt="gallery_section">
                        </a>
                     </div>
                  </div>
               @endif
            @endforeach
         </div>
      </div>
   </section>
   <!-- Gallery Section End -->


   <!-- News Section Start -->
   <section id="news">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>What's New?</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row"> 
            @foreach ($news_section as $news_section)
               @if($news_section->status == 0)
                  <div class="col-md-6 col-lg-4" data-aos="zoom-in">
                     <div class="news-1 h-100">
                        <a href="../uploads/news/{{ $news_section->image }}">
                           <img src="../uploads/news/{{ $news_section->image }}" class="w-100 img-thumbnail" alt="news_section" style="height:270px;width:470px;">
                        </a>
                        <h3 class="title-sm text-uppercase mt-4 mb-4 fw-bold">{{ $news_section->news_headline }}</h3>
                        <p>{{ $news_section->news_content }}</p>
                     </div>
                  </div>
               @endif
            @endforeach
                  <!--<div class="col-md-6 col-lg-4" data-aos="zoom-in">
                     <div class="news-1 h-100">
                        <a href=".">
                           <img src="assets/philippine_inquirer.jpg" class="w-100 img-thumbnail" alt="news_section" style="height:270px;width:470px;">
                        </a>
                        <a href="https://www.inquirer.net/" target="_blank" style="text-decoration:none; color:black;">
                           <h3 class="title-sm text-uppercase mt-4 mb-4 fw-bold">Philippine Daily Inquirer <i class='bx bx-right-arrow-alt'></i></h3>
                        </a>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-4" data-aos="zoom-in">
                     <div class="news-1 h-100">
                        <a href=".">
                           <img src="assets/philippine_star.jpg" class="w-100 img-thumbnail" alt="news_section" style="height:270px;width:470px;">
                        </a>
                        <a href="https://www.philstar.com/" target="_blank" style="text-decoration:none; color:black;">
                           <h3 class="title-sm text-uppercase mt-4 mb-4 fw-bold">Philippine Star <i class='bx bx-right-arrow-alt'></i></h3>
                        </a>
                     </div>
                  </div>-->
         </div>
         <!--<a href="" class="button mt-4" style="float: right;">VIEW ONLINE NEWS <i class='bx bx-right-arrow-alt'></i></a>-->
      </div>
   </section>
   <!-- News Section End -->


   <!-- Contact Us Section Start -->
   <section id="contact">
      <div class="container">
         <div class="row">
            <div class="col-12 section-intro" data-aos="zoom-in">
               <h1>Get In Touch</h1>
               <div class="hline"></div>
            </div>
         </div>
         <div class="row align-items-center" data-aos="zoom-in">
            <div class="col-lg-4">
               <img src="assets/contact.jpg" alt="" id="contact">
            </div>
            <div class="col-lg-6 offset-lg-1">
               <form action = "/submitContactForm" method = "post">
                  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                  <div class="mb-3">
                    <small>Full Name</small>
                    <input type="text" class="form-control mt-1 @error('fullname') is-invalid @enderror" id="fullname" name="fullname" placeholder="Enter full name" value="{{ old('fullname') }}">
                           @error('fullname')
                              <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                              </span>
                           @enderror
                  </div>
                  <div class="mb-3">
                     <small>Email Address</small>
                     <input type="email" class="form-control mt-1 @error('email') is-invalid @enderror" id="email" name="email" placeholder="Enter email address" value="{{ old('email') }}">
                           @error('email')
                              <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                              </span>
                           @enderror
                   </div>
                   <div class="mb-3">
                     <small>Subject</small>
                     <input type="text" class="form-control mt-1 @error('subject') is-invalid @enderror" id="subject" name="subject" placeholder="Enter subject" value="{{ old('subject') }}">
                           @error('subject')
                              <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                              </span>
                           @enderror
                   </div>
                   <div class="mb-3">
                     <small>Message</small>
                     <textarea name="message" id="message" cols="30" rows="7" class="form-control mt-1 @error('message') is-invalid @enderror" placeholder="Enter message" value="{{ old('message') }}"></textarea>
                           @error('message')
                              <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                              </span>
                           @enderror 
                  </div>
                  <input type="hidden" id="message_date" name="message_date" value="{{ now()->format('Y-m-d') }}">
                  <input type="hidden" id="message_time" name="message_time" value="<?php  date_default_timezone_set("Asia/Manila"); echo "" .date("h:i:s"). ""; ?>">
                  <input type="submit" class="button" name="set" value="SUBMIT">
                </form>
            </div>
         </div>
      </div>
   </section>
   <!-- Contact Us Section End -->


   <!-- Call to Action Section Start -->
   <section id="cta" class="py-5">
      <div class="container text-center py-4">
         <div class="row justify-content-center">
            <div class="col-md-8">
               <h3 class="text-black">Let's read, explore and discover together.</h3>
            </div>
            <div class="col-auto">
               <a href="{{ route('register') }}" class="btn btn-success">Join Us For Free</a>
            </div>
         </div>
      </div>
   </section>
   <!-- Call to Action Section End -->


   <!-- Footer Section Start -->
   <footer>
      <div class="footer-top">
         <div class="container text-center text-md-start">
            <div class="row">
               <div class="col-md-3 col-lg-4 col-xl-3 mx-auto">
                  <h6 class="text-uppercase fw-bold mb-4">
                     Villasis Municipal Library
                  </h6>
                  <p>
                     Villasis Municipal Library does not only cater readers and researchers but it also offers 
                     e-government services catering clients not only from the municipality but from neighboring 
                     towns as well as abroad.
                  </p>
               </div>

               <div class="col-md-2 col-lg-2 col-xl-2 mx-auto">
                  <h6 class="text-uppercase fw-bold mb-4">
                     Navigation
                  </h6>
                  <div class="footer-links">
                     <a href="#home">Home</a>
                     <a href="#about">About Us</a>
                     <a href="#services">Services</a>
                     <a href="#programs">Programs</a>
                     <a href="#awards">Awards</a>
                     <a href="#gallery">Gallery</a>
                     <a href="#news">News</a>
                     <a href="#contact">Contact Us</a>
                  </div>
               </div>
     
               <div class="col-md-4 col-lg-2 col-xl-3 mx-auto">
                  <h6 class="text-uppercase fw-bold mb-4">
                     Contact Us
                  </h6>
                  <div class="footer-links">
                     <p><a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=villasis.mzambrano@gmail.com" target="_blank"><i class="fa fa-envelope me-2"></i>villasis.mzambrano@gmail.com</a></p>
                     <p><a href="https://www.facebook.com/profile.php?id=100064414010842" target="_blank"><i class="fa fa-facebook me-2"></i>@Villasis Municipal Library</a></p>
                     <p><a href="tel:09171365509"><i class="fa fa-phone me-2"></i>0917 130 7677</a></p>
                     <p><a href="tel:0756324974"><i class="fa fa-phone me-2"></i>(075) 632 4974</a></p>
                  </div>
               </div>
    
               <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0">
                  <h6 class="text-uppercase fw-bold mb-4">
                     Visit Us
                  </h6>
                  <div class="footer-links">
                     <p><a href="https://www.google.fr/maps/place/Beatriz+De+Vera+Olivar+Legislative+Building/@15.9015667,120.587211,19z/data=!3m1!4b1!4m13!1m7!3m6!1s0x3391391a2128460b:0x217c306a3c06368c!2sVillasis,+Pangasinan!3b1!8m2!3d15.9060728!4d120.5852756!3m4!1s0x3391393d05120c31:0xd08b5f0cdfdfcc20!8m2!3d15.9015673!4d120.5877631" target="_blank"><i class="fa fa-home me-2"></i>Beatriz De Vera-Olivar Legislative Bldg., Poblacion Zone I</a></p>
                  </div>
                  <a href="qr-code">
                     <img src="assets/villasis_library_qrcode.svg" 
                            alt="villasis_library_qrcode" 
                            class="img-fluid" 
                            style="max-height: 100px;"/>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="container text-center text-md-start">
            <div class="row justify-content-between gy-3">
                  <div class="col-md-6">
                     <p>Copyright © <?php $year = date("Y");  echo $year; ?>. All Rights Reserved</p>
                  </div>
                  <div class="col-auto">
                     <p class="mb-0">Made with &#128155; by <a href="https://bio.link/jebsonubaldo" target="_blank" style="text-decoration:none; color:black;">Jebson Ubaldo</a></p>
                  </div>
            </div>
         </div>
      </div>   
   </footer>
   <!-- Footer Section End -->

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
   <script>
      AOS.init();
   </script>
</body>
</html>