@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/home" style="text-decoration: none;">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Borrow Books</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Borrow Book</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/borrowBook" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="title">Book Title</label>
                                        <input type="text" class="form-control mt-1 @error('title') is-invalid @enderror" id="title" name="title" placeholder="Enter title" value="{{ old('title') }}">
                                            @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="author">Book Author</label>
                                        <input type="text" class="form-control mt-1 @error('author') is-invalid @enderror" id="author" name="author" placeholder="Enter book author" value="{{ old('author') }}">
                                            @error('author')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="copyright">Book Copyright</label>
                                        <input type="text" class="form-control mt-1 @error('copyright') is-invalid @enderror" id="copyright" name="copyright" placeholder="Enter book copyright" value="{{ old('copyright') }}">
                                                @error('copyright')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="publisher">Book Publisher</label>
                                        <input type="text" class="form-control mt-1 @error('publisher') is-invalid @enderror" id="publisher" name="publisher" placeholder="Enter book publisher" value="{{ old('publisher') }}">
                                                @error('publisher')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                    </div>
                                </div>
                                <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" id="name" name="name" value="{{ Auth::user()->name }}">
                                <input type="hidden" id="email" name="email" value="{{ Auth::user()->email }}">
                                <input type="hidden" id="status" name="status" value="0">
                                <input type="hidden" id="remarks" name="remarks" value="For Approval">
                                <input type="hidden" id="borrow_date" name="borrow_date" value="<?php echo date('Y-m-d');?>">
                                <input type="hidden" id="return_date" name="return_date" value="<?php echo date('Y-m-d', strtotime(' + 5 days'));?>">
                                <button type="submit" class="btn btn-primary mt-4" name="set">Borrow Book</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>List of Borrowed Books</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <table class="table align-middle mb-0 bg-white" id="example">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Book Details</th>
                                            <th>Borrowed Date</th>
                                            <th>Returned Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($borrow as $borrow)
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div>
                                                            <p class="fw-bold mb-1">{{ $borrow->title }}</p>
                                                            <p class="text-muted mb-0">{{ $borrow->author }}</p>
                                                            <p class="text-muted mb-0">{{ $borrow->publisher }}</p>
                                                            <p class="text-muted mb-0">{{ $borrow->copyright }}</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        ---
                                                    @elseif ($borrow->status == 1)
                                                        {{ date('F d, Y', strtotime($borrow->borrow_date)) }}
                                                    @elseif ($borrow->status == 2)
                                                        ---
                                                    @endif 
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        ---
                                                    @elseif ($borrow->status == 1)
                                                        {{ date('F d, Y', strtotime($borrow->return_date)) }}
                                                    @elseif ($borrow->status == 2)
                                                        ---
                                                    @endif 
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        <span class="badge bg-warning">Pending</span>
                                                    @elseif ($borrow->status == 1)
                                                        <span class="badge bg-success">Approved</span>
                                                    @elseif ($borrow->status == 2)
                                                        <span class="badge bg-danger">Declined</span>
                                                        <p class="text-muted mb-0">{{ $borrow->remarks }}</p>
                                                    @endif   
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#cancelModal">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>
                                                    @else
                                                        <button type="button" class="btn btn-danger" disabled>
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>
                                                    @endif 
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Cancel Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Borrow Book</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to cancel borrowing book ?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/cancelBorrowBook/{{ $borrow->id}}" style="text-decoration:none; color:white;">Cancel Borrow Book</a></button>              
                </div>
            </form>
        </div>
    </div>
</div>
@endsection