<!DOCTYPE html>
<html lang="en">
<head>
   <!-- Meta Tags -->
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <!-- CSS -->
   <link rel="stylesheet" href="../css/client.css">

   <!-- Bootstrap CSS -->
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

   <!-- Font -->
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

   <!-- Icons -->
   <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <title>Login Form</title>
   <link rel="icon" type="image/png" href="../assets/villasis_library_logo.png">
</head>
<body>
    <!-- Navigation Bar Section Start -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white">
      <div class="container">
        <a class="navbar-brand" href="#">
            <img src="../assets/villasis_library_logo.png" 
                 alt="villasis_library_logo" 
                 class="img-fluid" 
                 height="50px" width="55px" style="margin-right: 7px;"/>
            <b style="font-family: 'Nunito', sans-serif;">Silid Booklatan</b>
        </a>
      </div>
    </nav>
    <!-- Navigation Bar Section End -->


    <!-- Login Section Start -->
    <section id="login">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-lg-5">
               <img src="../assets/login.svg" alt="login" style="width:450px; height:350px;">
            </div>
            <div class="col-lg-4 offset-lg-1">
               <div class="row">
                  <div class="col-12 section-intro">
                    <h1>Login Form</h1>
                    <div class="hline"></div>
                  </div>
               </div>
               <form method="POST" action="{{ route('login') }}">
                  @csrf

                  <!--<div class="mb-4">
                    <small>{{ __('Email Address') }}</small>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Enter email address" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>-->
                  <div class="mb-4">
                     <small>{{ __('Username') }}</small>
                     <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter username" autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                  <div class="mb-4">
                     <small>{{ __('Password') }}</small>
                     <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter password" autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                  <div class="form-group mt-2 mb-4">
                     <a href="{{ route('password.request') }}">Forgot Password</a><br>
                  </div>
                  <div class="form-group mb-2">
                    <button type="submit" class="button me-2 mt-4">
                        <small>{{ __('Login') }}</small>
                    </button>
                    Not yet a Member? <a href="{{ route('register') }}">Register</a>
                  </div>
                </form>
            </div>
         </div>
      </div>
    </section>
    <!-- Login Section End -->


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>