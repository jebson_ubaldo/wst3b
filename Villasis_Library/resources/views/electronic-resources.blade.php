@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Electronic Resources</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>Electronic Resources</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <table class="table align-middle mb-0 bg-white" id="example">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Electronic Resource Name</th>
                                            <th>Electronic Resource Link</th>
                                            <th>Image</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($electronic_resources as $electronic_resources)
                                            <tr>
                                                <td>{{ $electronic_resources->name }}</td>
                                                <td><a href="{{ $electronic_resources->link }}" target="_blank">{{ $electronic_resources->link }}</a></td>
                                                <td><a href="../uploads/electronic-resources/{{ $electronic_resources->image }}"><img src="../uploads/electronic-resources/{{ $electronic_resources->image }}" width="50px" height="50px" alt="image"></a></td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection