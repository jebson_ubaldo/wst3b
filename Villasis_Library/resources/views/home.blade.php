@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>Welcome to User Dashboard</h5></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
            
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-3 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="books" style="text-decoration: none; color:black;"><img src="../assets/books.svg" alt="books" style="width:150px; height:100px;"></a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="books" style="text-decoration: none; color:black;">List of Books</a></h5>
                            </div> 
                            <div class="col-md-6 col-lg-3 mb-4">
                                <div class="icon my-3 fs-2">
                                    <img src="../assets/website.svg" alt="electronic_resource" style="width:150px; height:100px;">
                                </div>
                                <h5 class="title-sm mt-4"><a href="electronic-resources" style="text-decoration: none; color:black;">Electronic Resources</a></h5>
                            </div>
                            <div class="col-md-6 col-lg-3 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="borrow-books/{{ Auth::user()->id }}" style="text-decoration: none; color:black;"><img src="../assets/borrow.svg" alt="books" style="width:150px; height:100px;"></a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="borrow-books/{{ Auth::user()->id }}" style="text-decoration: none; color:black;">Borrow Books</a></h5>
                            </div>  
                            <div class="col-md-6 col-lg-3 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="appointment/{{ Auth::user()->id }}"><img src="../assets/appointment.svg" alt="appointment" style="width:150px; height:100px;"></a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="appointment/{{ Auth::user()->id }}" style="text-decoration: none; color:black;">Book Appointment</a></h5>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection