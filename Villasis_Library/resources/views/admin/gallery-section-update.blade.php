@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item"><a href="/admin/gallery-section" style="text-decoration: none;">Gallery Section</a></li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-warning text-black">
                    <h5>Update Gallery Content</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/gallery-section-update/<?php echo $gallery_section[0]->id; ?>" method = "post"  enctype="multipart/form-data">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="details">Details</label>
                                        <input type="text" class="form-control mt-1 @error('details') is-invalid @enderror" id="details" name="details" value="<?php echo$gallery_section[0]->details; ?>">
                                            @error('details')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="content">Image</label>
                                        <textarea name="content" id="content" class="ckeditor form-control mt-1 @error('content') is-invalid @enderror" value="<?php echo$gallery_section[0]->image; ?>">
                                            <?php echo$gallery_section[0]->image; ?>
                                        </textarea>
                                            @error('content')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-warning mt-4" name="set" value="Update About Us Content"><br>
                                <a href="/admin/gallery-section" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>
@endsection