@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="admin/appointment" style="text-decoration: none;">Appointments</a></li>
            <li class="breadcrumb-item active" aria-current="page">Approve Borrow Books</li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-success text-white">
                    <h5>Approve Appointment</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/appointment-approve/<?php echo $appointment[0]->id; ?>" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" value="<?php echo$appointment[0]->name; ?>">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="service_availed">Service Availed</label>
                                        <input type="text" class="form-control mt-1 @error('service_availed') is-invalid @enderror" id="service_availed" name="service_availed" value="<?php echo$appointment[0]->service_availed; ?>">
                                            @error('service_availed')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="appointment_date">Appointment Date</label>
                                        <input type="date" class="form-control mt-1 @error('appointment_date') is-invalid @enderror" id="appointment_date" name="appointment_date" value="<?php echo$appointment[0]->appointment_date; ?>">
                                            @error('appointment_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="appointment_time">Appointment Time</label>
                                        <input type="text" class="form-control mt-1 @error('appointment_time') is-invalid @enderror" id="appointment_time" name="appointment_time" value="<?php echo$appointment[0]->appointment_time; ?>">
                                            @error('appointment_time')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="hidden" id="status" name="status" value="1">
                                <button type="submit" class="btn btn-success mt-4" name="set">Approve Borrow Book</button><br>
                                <a href="/admin/appointment" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection