@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item"><a href="/admin/awards-section" style="text-decoration: none;">Awards Section</a></li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <h5>Add Award</h5>
                </div>
                <div class="card-body">
                    <div class="container">
                        <form method="post" action="{{ url('awards_section') }}" enctype="multipart/form-data">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            @csrf
                            <div class="form-group row">
                                <!--<div class="mb-2"> 
                                    <label for="details">Award Details</label>
                                    <textarea name="details" id="details" cols="30" rows="5" class="form-control mt-1 @error('details') is-invalid @enderror" placeholder="Enter award details" value="{{ old('details') }}">
                                        {{ old('details') }}
                                    </textarea>
                                        @error('details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>-->
                                <div class="mb-2"> 
                                        <label for="details">Award Details</label>
                                        <input type="text" class="form-control mt-1 @error('details') is-invalid @enderror" id="details" name="details" placeholder="Enter award details" value="{{ old('details') }}">
                                                @error('details')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                </div>
                          
                                <div class="mb-2"> 
                                    <label for="image">Award Image</label>
                                    <input type="file" class="form-control mt-1 @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}">
                                            @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                </div>
                            </div>
                            <input type="hidden" id="status" name="status" value="0">
                            <button type="submit" class="btn btn-primary mt-4" name="set">Add Award</button><br>
                            <a href="/admin/awards-section" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection