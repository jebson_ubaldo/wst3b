@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item"><a href="/admin/services-section" style="text-decoration: none;">Services Section</a></li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <h5>Add Services</h5>
                </div>
                <div class="card-body">
                    <div class="container">
                        <form method="post" action="/addServices"  enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="name">Service Name</label>
                                    <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter service name" value="{{ old('name') }}">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="details">Service Details</label>
                                    <textarea name="details" id="details" class="ckeditor form-control mt-1 @error('details') is-invalid @enderror" placeholder="Enter service details" value="{{ old('details') }}"></textarea>
                                        @error('details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="icon">Service Icon</label>
                                    <input type="text" class="form-control mt-1 @error('icon') is-invalid @enderror" id="icon" name="icon" placeholder="Enter service icon" value="{{ old('icon') }}">
                                        @error('icon')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    <p class="mt-2"><i>Refer to this site for the list of available icons. </i><a href="https://boxicons.com/" target="_blank">https://boxicons.com/</a></p> 
                                </div>
                            </div>
                            <input type="hidden" id="status" name="status" value="0">
                            <button type="submit" class="btn btn-primary mt-4" name="set">Add Service</button><br>
                            <a href="/admin/services-section" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection