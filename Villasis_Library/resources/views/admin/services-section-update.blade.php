@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item"><a href="/admin/services-section" style="text-decoration: none;">Services Section</a></li>
        </ol>
    </nav>
    <div class="row justify-content-start">
    <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-warning text-black">
                    <h5>Update Services</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/services-section-update/<?php echo $services_section[0]->id; ?>" method = "post"  enctype="multipart/form-data">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="name">Service Name</label>
                                        <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" value="<?php echo$services_section[0]->name; ?>">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="details">Service Details</label>
                                        <textarea name="details" id="content" class="ckeditor form-control mt-1 @error('details') is-invalid @enderror" value="<?php echo$services_section[0]->details; ?>">
                                            <?php echo$services_section[0]->details; ?>
                                        </textarea>
                                            @error('details')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="icon">Icon</label>
                                        <input type="text" class="form-control mt-1 @error('icon') is-invalid @enderror" id="icon" name="icon" value="<?php echo$services_section[0]->icon; ?>" style="height:50px;">
                                            @error('icon')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-warning mt-4" name="set" value="Update Service"><br>
                                <a href="/admin/services-section" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>
@endsection