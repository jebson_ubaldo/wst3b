@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manage Website</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>Manage Website</h5></div>
                <div class="card-body">         
                    <div class="container">                
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="about-section"><i class="fa fa-info" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="about-section" style="text-decoration: none; color:black;">
                                        About Us Section
                                    </a>
                                </h6>
                            </div>
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="services-section"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="services-section" style="text-decoration: none; color:black;">
                                        Services Section
                                    </a>
                                </h6>
                            </div>
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="programs-section"><i class="fa fa-tasks" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="programs-section" style="text-decoration: none; color:black;">
                                        Programs Section
                                    </a>
                                </h6>
                            </div>
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="awards-section"><i class="fa fa-trophy" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="awards-section" style="text-decoration: none; color:black;">
                                        Award Section
                                    </a>
                                </h6>
                            </div>
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="gallery-section"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="gallery-section" style="text-decoration: none; color:black;">
                                        Gallery Section
                                    </a>
                                </h6>
                            </div>   
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="news-section"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="news-section" style="text-decoration: none; color:black;">
                                        News Section
                                    </a>
                                </h6>
                            </div> 
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="contact-form"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                                </div>
                                <h6 class="title-sm">
                                    <a href="contact-form" style="text-decoration: none; color:black;">
                                        Contact Us Section
                                    </a>
                                </h6>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection