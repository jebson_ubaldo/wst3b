@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/electronic-resources" style="text-decoration: none;">Electronic Resources</a></li>
            <li class="breadcrumb-item active" aria-current="page">Update Electronic Resource</li>
        </ol>
    </nav>
    <div class="row justify-content-start">
    <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-warning text-black">
                    <h5>Update Electronic Resource</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/electronic-resources-update/<?php echo $electronic_resources[0]->id; ?>" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="name">Electronic Resource Name</label>
                                        <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter electronic resource name" value="<?php echo$electronic_resources[0]->name; ?>" required>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="link">Electronic Resource Link</label>
                                        <input type="text" class="form-control mt-1 @error('link') is-invalid @enderror" id="link" name="link" placeholder="Enter electronic resource link" value="<?php echo$electronic_resources[0]->link; ?>" required>
                                            @error('link')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div> 
                                <input type="submit" class="btn btn-warning mt-4" name="set" value="Update Electronic Resource"><br>
                                <a href="/admin/electronic-resources" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>
@endsection