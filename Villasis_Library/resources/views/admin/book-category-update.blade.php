@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Book Categories</li>
        </ol>
    </nav>
    <div class="row justify-content-start">
    <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-warning text-black">
                    <h5>Update Book Category</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/book-category-update/<?php echo $book_category[0]->id; ?>" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="category_name">Category Name</label>
                                        <input type="text" class="form-control mt-1 @error('category_name') is-invalid @enderror" id="category_name" name="category_name" placeholder="Enter book category" value="<?php echo$book_category[0]->category_name; ?>">
                                            @error('category_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-warning mt-4" name="set" value="Update Book Category"><br>
                                <a href="/admin/book-category" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
@endsection