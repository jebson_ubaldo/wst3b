@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item active" aria-current="page">News Section</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <!--<div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Add News</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/addNews" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="news_headline">News Headline</label>
                                        <textarea name="news_headline" id="news_headline" class="form-control mt-1 @error('news_headline') is-invalid @enderror" placeholder="Enter news headline" value="{{ old('news_headline') }}" cols="30" rows="3"></textarea>
                                            @error('news_headline')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="news_content">News Content</label>
                                        <textarea name="news_content" id="news_content" class="form-control mt-1 @error('news_content') is-invalid @enderror" placeholder="Enter news content" value="{{ old('news_content') }}" cols="30" rows="6"></textarea>
                                            @error('news_content')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="image">News Image</label>
                                        <input type="file" class="form-control mt-1 @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}">
                                                @error('image')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary mt-4" name="set">Add News</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>List of News</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                            <div class="card-body table-responsive">
                                <a href="/news-section-add" class="btn btn-primary mb-4">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add News
                                </a>
                                <table class="table align-middle mb-0 bg-white" id="example">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>News Headline</th>
                                            <th>News Content</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($news_section as $news_section)
                                            <tr>
                                                <td>{{ $news_section->news_headline }}</td>
                                                <td>{{ $news_section->news_content }}</td>
                                                <td><a href="../uploads/news/{{ $news_section->image }}"><img src="../uploads/news/{{ $news_section->image }}" width="70px" height="50px" alt="image"></a></td>
                                                <td>
                                                    @if($news_section->status == 0)
                                                        <a href="/showNewsSection/{{ $news_section->id }}" class="btn btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Hide Content">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                    @elseif ($news_section->status == 1)
                                                        <a href="/hideNewsSection/{{ $news_section->id }}" class="btn btn-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Show Content">
                                                            <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    <!--<a href="/news-section-update/{{ $news_section->id }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
                                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete Content">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action = "/addNews" method = "post">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Add News</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-4">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="news_headline">News Headline</label>
                            <textarea name="news_headline" id="news_headline" class="form-control mt-1 @error('news_headline') is-invalid @enderror" placeholder="Enter news headline" value="{{ old('news_headline') }}" cols="30" rows="3"></textarea>
                                @error('news_headline')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="news_content">News Content</label>
                            <textarea name="news_content" id="news_content" class="form-control mt-1 @error('news_content') is-invalid @enderror" placeholder="Enter news content" value="{{ old('news_content') }}" cols="30" rows="6"></textarea>
                                @error('news_content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="image">News Image</label>
                            <input type="file" class="form-control mt-1 @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}">
                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" name="set">Add News</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Delete News</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to delete ?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/deleteNewsSection/{{ $news_section->id}}" style="text-decoration:none; color:white;">Delete News</a></button>
                </div> 
            </form>
        </div>
    </div>
</div>
@endsection