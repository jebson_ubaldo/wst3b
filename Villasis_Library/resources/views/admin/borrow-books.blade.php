@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manage Borrowed Books</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>List of Borrowed Books</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <table class="table align-middle mb-0 bg-white" id="borrow-books">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Name</th>
                                            <th>Book Details</th>
                                            <th>Borrowed Date</th>
                                            <th>Returned Date</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($borrow as $borrow)
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div>
                                                            <p class="fw-bold mb-1">{{ $borrow->name }}</p>
                                                            <p class="text-muted mb-0">{{ $borrow->email }}</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="fw-normal mb-1">{{ $borrow->title }}</p>
                                                    <p class="text-muted mb-0">{{ $borrow->author }}</p>
                                                    <p class="text-muted mb-0">{{ $borrow->publisher }}</p>
                                                    <p class="text-muted mb-0">{{ $borrow->copyright }}</p>
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        ---
                                                    @elseif ($borrow->status == 1)
                                                        {{ date('F d, Y', strtotime($borrow->borrow_date)) }}
                                                    @elseif ($borrow->status == 2)
                                                        ---
                                                    @endif 
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        ---
                                                    @elseif ($borrow->status == 1)
                                                        {{ date('F d, Y', strtotime($borrow->return_date)) }}
                                                    @elseif ($borrow->status == 2)
                                                        ---
                                                    @endif 
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        <span class="badge bg-warning">Pending</span>
                                                    @elseif ($borrow->status == 1)
                                                        <span class="badge bg-success">Approved</span>
                                                    @elseif ($borrow->status == 2)
                                                        <span class="badge bg-danger">Declined</span>
                                                    @endif  
                                                </td>
                                                <td>
                                                    @if($borrow->status == 0)
                                                        <!--<button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#approveModal">
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#declineModal">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>-->
                                                        <a href="/borrow-books-approve/{{ $borrow->id }}" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i></a>
                                                        <a href="/borrow-books-decline/{{ $borrow->id }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                    @else
                                                        <button type="button" class="btn btn-success" disabled>
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-danger" disabled>
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>
                                                    @endif 
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Approve Modal -->
<div class="modal fade" id="approveModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header bg-success text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Approve Borrow Book</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to approve the borrowed book?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success" name="set"><a href="/approveBorrowBook/{{ $borrow->id}}" style="text-decoration:none; color:white;">Approve Borrowed Book</a></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Decline Modal -->
<div class="modal fade" id="declineModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Decline Borrow Book</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to decline the borrowed book?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/declineBorrowBook/{{ $borrow->id}}" style="text-decoration:none; color:white;">Decline Borrowed Book</a></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection