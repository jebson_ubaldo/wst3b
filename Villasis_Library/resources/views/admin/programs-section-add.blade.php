@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item"><a href="/admin/programs-section" style="text-decoration: none;">Programs Section</a></li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <h5>Add Programs</h5>
                </div>
                <div class="card-body">
                    <div class="container">
                        <form method="post" action="/addPrograms" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="name">Program Name</label>
                                    <textarea name="name" id="content" cols="30" rows="5" class="ckeditor form-control mt-1 @error('name') is-invalid @enderror" placeholder="Enter program name" value="{{ old('name') }}">{{ old('name') }}</textarea>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <input type="hidden" id="status" name="status" value="0">
                            <button type="submit" class="btn btn-primary mt-4" name="set">Add Program</button><br>
                            <a href="/admin/programs-section" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection