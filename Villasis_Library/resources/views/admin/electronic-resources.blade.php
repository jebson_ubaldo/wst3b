@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Electronic Resources</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>Electronic Resources</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <button type="button" class="btn btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#addModal">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Add Electronic Resource
                                </button>
                                <table class="table align-middle mb-0 bg-white" id="e-resources">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Electronic Resource Name</th>
                                            <th>Electronic Resource Link</th>
                                            <th>Image</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($electronic_resources as $electronic_resources)
                                            <tr>
                                                <td>{{ $electronic_resources->name }}</td>
                                                <td><a href="{{ $electronic_resources->link }}" target="_blank">{{ $electronic_resources->link }}</a></td>
                                                <td><a href="../uploads/electronic-resources/{{ $electronic_resources->image }}"><img src="../uploads/electronic-resources/{{ $electronic_resources->image }}" width="50px" height="50px" alt="image"></a></td>
                                                <td>
                                                    <a href="/electronic-resources-update/{{ $electronic_resources->id }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{ url('electronic_resources') }}" enctype="multipart/form-data">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Add Electronic Resource</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-4">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="name">Electronic Resource Name</label>
                            <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter electronic resource name" value="{{ old('name') }}" required>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="link">Electronic Resource Link</label>
                            <input type="text" class="form-control mt-1 @error('link') is-invalid @enderror" id="link" name="link" placeholder="Enter electronic resource link" value="{{ old('link') }}" required>
                                @error('link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div> 
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="image">Image</label>
                            <input type="file" class="form-control mt-1 @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}" required>
                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" name="set">Add Electronic Resource</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action = "/addBookCategory" method = "post">
                <div class="modal-header bg-warning text-black">
                    <h5 class="modal-title" id="exampleModalLabel">Update Electronic Resource</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-4">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <div class="form-group row">
                        <div class="mb-2"> 
                            <label for="category_name">Category Name</label>
                            <input type="text" class="form-control mt-1 @error('category_name') is-invalid @enderror" id="category_name" name="category_name" placeholder="Enter book category" value="{{ old('category_name') }}">
                                @error('category_name')
                                    <span class="invalid-feedback" role="alert">
                                        strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-warning" name="set"><a href="}" style="text-decoration:none; color:black;">Update Electronic Resource</a></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action = "/addBookCategory" method = "post">
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Electronic Resource</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to delete ?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/deleteElectronicResources/{{ $electronic_resources->id}}" style="text-decoration:none; color:white;">Delete Electronic Resource</a></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection