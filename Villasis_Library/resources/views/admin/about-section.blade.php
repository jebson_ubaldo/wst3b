@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item active" aria-current="page">About Us Section</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <!--<div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Add About Us Content</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/addAbout" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter about us name" value="{{ old('name') }}">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="content">News Content</label>
                                        <textarea name="content" id="content" class="form-control mt-1 @error('content') is-invalid @enderror" placeholder="Enter about us content" value="{{ old('content') }}" cols="30" rows="10"></textarea>
                                            @error('content')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary mt-4" name="set">Add About Us Content</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>About Us</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                            <div class="card-body table-responsive">
                                <a href="/about-section-add" class="btn btn-primary mb-4">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add About Us Content
                                </a>
                                <table class="table align-middle mb-0 bg-white" id="example">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Name</th>
                                            <th>Content</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($about_section as $about_section)
                                            <tr>
                                                <td><p class="fw-bold">{{ $about_section->name }}</p></td>
                                                <td>{{ $about_section->content }}</td>
                                                <td>
                                                    @if($about_section->status == 0)
                                                        <a href="/showAboutSection/{{ $about_section->id }}" class="btn btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Hide Content">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                    @elseif ($about_section->status == 1)
                                                        <a href="/hideAboutSection/{{ $about_section->id }}" class="btn btn-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Show Content">
                                                            <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    <a href="/about-section-update/{{ $about_section->id }}" class="btn btn-warning" data-bs-toggle="tooltip" data-bs-placement="top" title="Update Content">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>
                                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete Content">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Delete About Us Content</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to delete ?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/deleteAboutSection/{{ $about_section->id}}" style="text-decoration:none; color:white;">Delete About Us Content</a></button>
                </div> 
            </form>
        </div>
    </div>
</div>
@endsection