@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inquiries</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>Inquiries</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                    <table class="table align-middle mb-0 bg-white" id="contact-form">
                                        <thead class="bg-light">
                                            <tr>
                                                <th>Full Name</th>
                                                <th>Message Date & Time</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($contact_form as $contact_form)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <p class="fw-bold mb-1">{{ $contact_form->fullname }}</p>
                                                                <p class="text-muted mb-0"><a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to={{ $contact_form->email }}" target="_blank">{{ $contact_form->email }}</a></p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="fw-normal mb-1">{{ date('F d, Y', strtotime($contact_form->message_date)) }}</p>
                                                        <p class="text-muted mb-0">{{ $contact_form->message_time }}</p>
                                                    </td>
                                                    <td>{{ $contact_form->subject }}</td>
                                                    <td>{{ $contact_form->message }}</td>
                                                </tr>      
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection