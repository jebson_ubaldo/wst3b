@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="admin/borrow-books" style="text-decoration: none;">Borrow Books</a></li>
            <li class="breadcrumb-item active" aria-current="page">Decline Borrow Books</li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-danger text-white">
                    <h5>Decline Borrow Book</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/borrow-books-approve/<?php echo $borrow[0]->id; ?>" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control mt-1 @error('name') is-invalid @enderror" id="name" name="name" value="<?php echo$borrow[0]->name; ?>">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="email">Email Address</label>
                                        <input type="email" class="form-control mt-1 @error('email') is-invalid @enderror" id="email" name="email" value="<?php echo$borrow[0]->email; ?>">
                                            @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control mt-1 @error('title') is-invalid @enderror" id="title" name="title" value="<?php echo$borrow[0]->title; ?>">
                                            @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="author">Author</label>
                                        <input type="text" class="form-control mt-1 @error('author') is-invalid @enderror" id="author" name="author" value="<?php echo$borrow[0]->author; ?>">
                                            @error('author')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="publisher">Publisher</label>
                                        <input type="text" class="form-control mt-1 @error('publisher') is-invalid @enderror" id="publisher" name="publisher" value="<?php echo$borrow[0]->publisher; ?>">
                                            @error('publisher')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="copyright">Copyright</label>
                                        <input type="text" class="form-control mt-1 @error('copyright') is-invalid @enderror" id="copyright" name="copyright" value="<?php echo$borrow[0]->copyright; ?>">
                                            @error('copyright')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="borrow_date">Declined Date</label>
                                        <input type="date" class="form-control mt-1 @error('borrow_date') is-invalid @enderror" id="borrow_date" name="borrow_date" value="<?php echo date('Y-m-d');?>">
                                            @error('borrow_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="form-group col-md-6 mb-2"> 
                                        <label for="remarks">Remarks</label>
                                        <select class="form-control mt-1 @error('remarks') is-invalid @enderror" name="remarks" id="remarks" value="{{ old('remarks') }}">
                                            <option value="" selected>- SELECT -</option>
                                            <option value="Book is not available.">Book is not available.</option>
                                            <option value="Book borrowed by other user.">Book borrowed by other user.</option>
                                        </select>
                                            @error('remarks')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="hidden" id="borrow_date" name="borrow_date" value="<?php echo date('Y-m-d');?>">
                                <input type="hidden" id="return_date" name="return_date" value="<?php echo date('Y-m-d', strtotime(' + 5 days'));?>">
                                <input type="hidden" id="status" name="status" value="2">
                                <button type="submit" class="btn btn-danger mt-4" name="set">Decline Borrow Book</button><br>
                                <a href="/admin/borrow-books" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection