@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/books" style="text-decoration: none;">List of Books</a></li>
            <li class="breadcrumb-item active" aria-current="page">Update Book</li>
        </ol>
    </nav>
    <div class="row justify-content-start">
    <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-warning text-black">
                    <h5>Update Book Category</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/books-update/<?php echo $books[0]->id; ?>" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control mt-1 @error('title') is-invalid @enderror" id="title" name="title" value="<?php echo$books[0]->title; ?>">
                                            @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="book_category">Category Name</label>
                                        <select class="form-control mt-1" name="book_category" id="book_category" value="<?php echo$books[0]->book_category; ?>">
                                            <option value="<?php echo$books[0]->book_category; ?>" selected><?php echo$books[0]->book_category; ?></option>
                                            <option value="Filipiniana Section">Filipiniana Section</option>
                                            <option value="Children Section">Children Section</option>
                                            <option value="Reference Section">Reference Section</option>
                                            <option value="Local History Section">Local History Section</option>
                                        </select>
                                     
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="title">Author</label>
                                        <input type="text" class="form-control mt-1 @error('author') is-invalid @enderror" id="author" name="author" value="<?php echo$books[0]->author; ?>">
                                            @error('author')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="title">Publisher</label>
                                        <input type="text" class="form-control mt-1 @error('publisher') is-invalid @enderror" id="publisher" name="publisher" value="<?php echo$books[0]->publisher; ?>">
                                            @error('publisher')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="copyright">Copyright</label>
                                        <input type="text" class="form-control mt-1 @error('copyright') is-invalid @enderror" id="copyright" name="copyright" value="<?php echo$books[0]->copyright; ?>">
                                            @error('copyright')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="mb-2"> 
                                        <label for="total">No. of Books</label>
                                        <input type="number" class="form-control mt-1 @error('total') is-invalid @enderror" id="total" name="total" value="<?php echo$books[0]->total; ?>">
                                            @error('total')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-warning mt-4" name="set" value="Update Book Category"><br>
                                <a href="/admin/books" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
@endsection