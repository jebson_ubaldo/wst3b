@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Book Categories</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>Book Categories</h5></div>
                <div class="card-body">         
                    <div class="container">                
                        <div class="row text-center">
                            @foreach ($category as $book_category)
                                <div class="col-md-6 col-lg-2 mb-4">
                                    <div class="icon my-3 fs-2">
                                        <a href="{{ url('list-books/'.$book_category->id) }}"><i class='bx bxs-book' style="color: black"></i></a>
                                    </div>
                                    <h6 class="title-sm">
                                        <a href="{{ url('list-books/'.$book_category->id) }}" style="text-decoration: none; color:black;">
                                        {{ $book_category->category_name }} 
                                        </a>
                                    </h6>
                                </div>  
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection