@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/admin/manage-website" style="text-decoration: none;">Manage Website</a></li>
            <li class="breadcrumb-item"><a href="/admin/news-section" style="text-decoration: none;">News Section</a></li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <h5>Add News</h5>
                </div>
                <div class="card-body">
                    <div class="container">
                        <form method="post" action="{{ url('news_section') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="news_headline">News Headline</label>
                                    <input type="text" class="form-control mt-1 @error('news_headline') is-invalid @enderror" id="news_headline" name="news_headline" placeholder="Enter news headline" value="{{ old('news_headline') }}">
                                        @error('news_headline')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="news_content">News Content</label>
                                    <textarea name="news_content" id="news_content" class="form-control mt-1 @error('news_content') is-invalid @enderror" placeholder="Enter news content" value="{{ old('news_content') }}" cols="30" rows="6"></textarea>
                                        @error('news_content')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="mb-2"> 
                                    <label for="image">News Image</label>
                                    <input type="file" class="form-control mt-1 @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}">
                                        @error('image')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                            <input type="hidden" id="status" name="status" value="0">
                            <button type="submit" class="btn btn-primary mt-4" name="set">Add News</button><br>
                            <a href="/admin/news-section" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection