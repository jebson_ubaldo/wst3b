@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/admin/home" style="text-decoration: none;">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">List of Books</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>List of Books</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <button type="button" class="btn btn-primary mb-4" data-bs-toggle="modal" data-bs-target="#addModal">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Add Book
                                </button>

                                <table class="table align-middle mb-0 bg-white" id="books">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Title</th>
                                            <th>Book Category</th>
                                            <th>Author</th>
                                            <th>Publisher</th>
                                            <th>Copyright</th>
                                            <th>Available Books</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($books as $books)
                                            <tr>
                                                <td>{{ $books->title }}</td>
                                                <td>
                                                    @if($books->book_category == 'Filipiniana Section')
                                                        <span class="badge bg-success">Filipiniana Section</span>
                                                    @elseif($books->book_category == 'Children Section')
                                                        <span class="badge bg-warning">Children Section</span>
                                                    @elseif ($books->book_category == 'Reference Section')
                                                        <span class="badge bg-danger">Reference Section</span>
                                                    @elseif ($books->book_category == 'Local History Section')
                                                        <span class="badge bg-primary">Local History Section</span>
                                                    @endif 
                                                </td>
                                                <td>{{ $books->author }}</td>
                                                <td>{{ $books->publisher }}</td>
                                                <td>{{ $books->copyright }}</td>
                                                <td><center>{{ $books->total }}</center></td>
                                                <td><a href="../uploads/books/{{ $books->image }}"><img src="../uploads/books/{{ $books->image }}" width="50px" height="60px" alt="image"></a></td>
                                                <td>
                                                    <a href="/books-update/{{ $books->id }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="post" action="{{ url('books') }}" enctype="multipart/form-data">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Add Book</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-4">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    @csrf
                    <div class="mb-2"> 
                        <label for="title">Title</label>
                        <input type="text" class="form-control mt-1 @error('title') is-invalid @enderror" id="title" name="title" placeholder="Enter title" value="{{ old('title') }}" required>
                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                    <div class="mb-2"> 
                        <label for="book_category">Category Name</label>
                            <select class="form-control mt-1 @error('book_category') is-invalid @enderror" name="book_category" id="book_category" value="{{ old('book_category') }}" required>
                                <option value="" selected>- SELECT -</option>
                                <option value="Filipiniana Section">Filipiniana Section</option>
                                <option value="Children Section">Children Section</option>
                                <option value="Reference Section">Reference Section</option>
                                <option value="Local History Section">Local History Section</option>
                            </select>
                                @error('book_category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="mb-2"> 
                        <label for="author">Author</label>
                            <input type="text" class="form-control mt-1 @error('author') is-invalid @enderror" id="author" name="author" placeholder="Enter author" value="{{ old('author') }}" required>
                                @error('author')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="mb-2"> 
                        <label for="copyright">Copyright</label>
                        <input type="text" class="form-control mt-1 @error('copyright') is-invalid @enderror" id="copyright" name="copyright" placeholder="Enter copyright" value="{{ old('copyright') }}" required>
                                @error('copyright')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="mb-2"> 
                        <label for="total">No. of Books</label>
                        <input type="number" class="form-control mt-1 @error('total') is-invalid @enderror" id="total" name="total" placeholder="Enter no. of books" value="{{ old('total') }}" required>
                                @error('total')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="mb-2"> 
                        <label for="publisher">Publisher</label>
                        <input type="text" class="form-control mt-1 @error('publisher') is-invalid @enderror" id="publisher" name="publisher" placeholder="Enter publisher" value="{{ old('publisher') }}" required>
                                @error('publisher')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="mb-2"> 
                        <label for="image">Image</label>
                        <input type="file" class="form-control mt-1 @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}" required>
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" name="set">Add Book</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Book</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to delete ?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/deleteBook/{{ $books->id}}" style="text-decoration:none; color:white;">Delete Book</a></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection