<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Villasis Municipal Library</title>
    <link rel="icon" type="image/png" href="../assets/villasis_library_logo.png">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Icons -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Datatables -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>    
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>    
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>    
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>    
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js"></script>

    <!-- Internal CSS -->
    <style>
        div.dataTables_filter, div.dataTables_length {
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .sorting_desc{background-image:url("../images/sort_desc.png")}

        table.dataTable thead .sorting_asc {
            background-image: url('../assets/') !important;
        }
        table.dataTable thead .sorting_desc {
            background-image: url('...url of descending image here...') !important;
        }

        .buttons-print {
            float: right;
        }

        .buttons-excel {
            float: right;
            margin-right: 5px
        }

        .buttons-pdf {
            float: right;
            margin-right: 5px
        }

        /* Scroll Bar */
        /* width */
        ::-webkit-scrollbar {
        width: 8px;
        }
        
        /* Track */
        ::-webkit-scrollbar-track {
        background: #fff; 
        }
        
        /* Handle */
        ::-webkit-scrollbar-thumb {
        background: #ffee93;
        border-radius: 5px;
        }
        
        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
        background: #ffd60a;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar  navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="../assets/villasis_library_logo.png" 
                        alt="villasis_library_logo" 
                        class="img-fluid" 
                        height="50px" width="55px" style="margin-right: 7px;"/>
                    <b style="font-family: 'Nunito', sans-serif;">Silid-Booklatan</b>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                'paging'      : false,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 15,
            });
        });
        $(document).ready(function () {
            $('#books').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"></i>',
                        messageTop: 'List of Available Books',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5 ]
                        }
                    },
                    
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-success',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        messageTop: 'List of Available Books',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5 ]
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        className: 'btn btn-danger',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        messageTop: 'List of Available Books',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5 ]
                        }
                    },
                    
                ],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 15,
            });
        });
        $(document).ready(function () {
            $('#e-resources').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"></i>',
                        messageTop: 'Electronic Resources',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    },
                    
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-success',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        messageTop: 'Electronic Resources',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        className: 'btn btn-danger',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        messageTop: 'Electronic Resources',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    },
                    
                ],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 15,
            });
        });
        $(document).ready(function () {
            $('#borrow-books').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"></i>',
                        messageTop: 'List of Borrowed Books',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-success',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        messageTop: 'List of Borrowed Books',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        className: 'btn btn-danger',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        messageTop: 'List of Borrowed Books',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    
                ],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 15,
            });
        });
        $(document).ready(function () {
            $('#appointment').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"></i>',
                        messageTop: 'E-Government Services Appointments',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-success',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        messageTop: 'E-Government Services Appointments',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        className: 'btn btn-danger',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        messageTop: 'E-Government Services Appointments',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    
                ],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 15,
            });
        });
        $(document).ready(function () {
            $('#contact-form').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"></i>',
                        messageTop: 'Inquiries',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-success',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        messageTop: 'Inquiries',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        className: 'btn btn-danger',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        messageTop: 'Inquiries',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    
                ],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 15,
            });
        });
        $(document).ready(function () {
            $('#clients').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"></i>',
                        messageTop: 'List of Clients',
                        exportOptions: {
                            columns: [ 0, 1, 2 ]
                        }
                    },
                    
                    {
                        extend: 'excelHtml5',
                        className: 'btn btn-success',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        messageTop: 'List of Clients',
                        exportOptions: {
                            columns: [ 0, 1, 2 ]
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        className: 'btn btn-danger',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        messageTop: 'List of Clients',
                        exportOptions: {
                            columns: [ 0, 1, 2 ]
                        }
                    },
                    
                ],
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : false,
                'pageLength'  : 15,
            });
        });
    </script>
</body>
</html>