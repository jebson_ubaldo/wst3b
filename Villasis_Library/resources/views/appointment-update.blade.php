@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/home" style="text-decoration: none;">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Book Appointment</li>
        </ol>
    </nav>
    <div class="row justify-content-start">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Update Appointment</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/appointment-update/<?php echo $appointments[0]->id; ?>" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2">
                                        <label for="service_availed">Service Availed</label>
                                        <select class="form-control mt-1 @error('service_availed') is-invalid @enderror" name="service_availed" id="service_availed" value="<?php echo$appointments[0]->service_availed; ?>">
                                            <option value="" selected>- SELECT -</option>
                                            <option value="PSA">PSA</option>
                                            <option value="DFA">DFA</option>
                                            <option value="NBI">NBI</option>
                                            <option value="PRC">PRC</option>
                                            <option value="POEA">POEA</option>
                                            <option value="SSS">SSS</option>
                                            <option value="GSIS">GSIS</option>
                                            <option value="PAGIBIG">PAGIBIG</option>
                                        </select>
                                            @error('service_availed')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="date">Date</label>
                                        <input type="date" class="form-control mt-1 @error('appointment_date') is-invalid @enderror" id="appointment_date" name="appointment_date" value="<?php echo$appointments[0]->appointment_date; ?>">
                                            @error('appointment_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="time">Time</label>
                                        <select class="form-control mt-1 @error('appointment_time') is-invalid @enderror" name="appointment_time" id="appointment_time" value="{<?php echo$appointments[0]->appointment_time; ?>">
                                            <option value="" selected>- SELECT</option>
                                            <option value="8:00 am - 9:00 am">8:00 am - 9:00 am</option>
                                            <option value="9:00 am - 10:00 am">9:00 am - 10:00 am</option>
                                            <option value="10:00 am - 11:00 am">10:00 am - 11:00 am</option>
                                            <option value="11:00 am - 12:00 am">11:00 am - 12:00 pm</option>
                                            <option value="1:00 pm - 2:00 pm">1:00 pm - 2:00 pm</option>
                                            <option value="2:00 pm - 3:00 pm">2:00 pm - 3:00 pm</option>
                                            <option value="3:00 pm - 4:00 pm">3:00 pm - 4:00 pm</option>
                                            <option value="4:00 pm - 5:00 pm">4:00 pm - 5:00 pm</option>
                                        </select>
                                            @error('appointment_time')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                              
                                <button type="submit" class="btn btn-warning mt-4" name="set">Update Appointment</button><br>
                                <a href="/home" class="btn btn-light mt-2" data-mdb-ripple-color="dark">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection