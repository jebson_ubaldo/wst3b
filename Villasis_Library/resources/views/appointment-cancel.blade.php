<!DOCTYPE html>
<html lang="en">
<head>
   <!-- Meta Tags -->
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <!-- CSS -->
   <link rel="stylesheet" href="../css/style.css">

   <!-- Bootstrap CSS -->
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

   <!-- Font -->
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">

   <!-- Icons -->
   <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <title>Villasis Municipal Library</title>
   <link rel="icon" type="image/png" href="assets/villasis_library_logo.png">
</head>
<body>
    <!-- Appointment Form Cancelled Section Start -->
    <section>
      <div class="container text-center">
         <div class="row justify-content-center">
            <div class="col-lg-12">
               <img src="../assets/cancel.svg" 
                  alt="404_page_not_found" 
                  class="img-fluid me-4 mb-1" 
                  height="240px" width="240px"/>
               <h5 class="mt-4 mb-4">Appointment Cancelled Successfully.</h5>
               <a href="/appointment/{{ Auth::user()->id }}" class="button mt-4">GO TO APPOINTMENT PAGE <i class='bx bx-right-arrow-alt'></i></a>
            </div>
         </div>
      </div>
    </section>
    <!-- Appointment Form Cancelled Section End -->  

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>