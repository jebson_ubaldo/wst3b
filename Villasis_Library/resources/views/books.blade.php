@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/home" style="text-decoration: none;">Home</a></li>
            <!--<li class="breadcrumb-item"><a href="/book-category" style="text-decoration: none;">Book Categories</a></li>-->
            <li class="breadcrumb-item active" aria-current="page">List of Books</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5>List of Books</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <h5 class="mb-4">
                                    <div style="text-align: center;">
                                        <div style="text-align: justify;" class="mb-1">
                                            <span style="text-align: left;"><b>NOTE:</b></span>
                                        </div>
                                        <span style="text-align: left;">This page  is were you can search all the available books and resources in Villasis Municipal Library. If you Find a book that you want to borrow then you can go in this link&nbsp;</span>
                                        <a href="/borrow-books/{{ Auth::user()->id }}">Borrow Book</a>     
                                        <span style="text-align: left;">to fill up the form.</span>
                                    </div>
                                </h5>
                                <table class="table align-middle mb-0 bg-white" id="example">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Title</th>
                                            <th>Book Category</th>
                                            <th>Author</th>
                                            <th>Publisher</th>
                                            <th>Copyright</th>
                                            <th>Image</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($books as $books)
                                            <tr>
                                                <td>{{ $books->title }}</td>
                                                <td>
                                                    @if($books->book_category == 'Filipiniana Section')
                                                        <span class="badge bg-success">Filipiniana Section</span>
                                                    @elseif($books->book_category == 'Children Section')
                                                        <span class="badge bg-warning">Children Section</span>
                                                    @elseif ($books->book_category == 'Reference Section')
                                                        <span class="badge bg-danger">Reference Section</span>
                                                    @elseif ($books->book_category == 'Local History Section')
                                                        <span class="badge bg-primary">Local History Section</span>
                                                    @endif 
                                                </td>
                                                <td>{{ $books->author }}</td>
                                                <td>{{ $books->publisher }}</td>
                                                <td>{{ $books->copyright }}</td>
                                                <td><a href="../uploads/books/{{ $books->image }}"><img src="../uploads/books/{{ $books->image }}" width="50px" height="60px" alt="image"></a></td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection