@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>
                        Welcome to Admin Dashboard <br>
                    </h5>
                    <p>
                        <?php
                            $mytime = Carbon\Carbon::now();
                            echo "Date: ".date('F d, Y', strtotime( $mytime->toDateTimeString()));
                            date_default_timezone_set("Asia/Manila");
                            echo "<br>Time: " .date("h:i:s"). "<br>";
                        ?>
                    </p>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
            
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="books" style="text-decoration: none; color:black;">
                                        <img src="../assets/books.svg" alt="appointment" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="books" style="text-decoration: none; color:black;">List of Books</a></h5>
                            </div>
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="electronic-resources" style="text-decoration: none; color:black;">
                                        <img src="../assets/electronic_resource.svg" alt="electronic-resources" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="electronic-resources" style="text-decoration: none; color:black;">Electronic Resources</a></h5>
                            </div>  
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="borrow-books" style="text-decoration: none; color:black;">
                                        <img src="../assets/borrow.svg" alt="electronic-resources" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="borrow-books" style="text-decoration: none; color:black;">Borrowed Books</a></h5>
                            </div> 
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="appointment" style="text-decoration: none; color:black;">
                                        <img src="../assets/appointment.svg" alt="appointment" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="appointment" style="text-decoration: none; color:black;">Appointments</a></h5>
                            </div>  
                            <!--<div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="book-category" style="text-decoration: none; color:black;">
                                        <img src="../assets/book_category.svg" alt="book-category" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="book-category" style="text-decoration: none; color:black;">Book Categories</a></h5>
                            </div>-->  
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="manage-website" style="text-decoration: none; color:black;">
                                        <img src="../assets/website.svg" alt="manage-website" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="manage-website" style="text-decoration: none; color:black;">Manage Website</a></h5>
                            </div>
                            <div class="col-md-6 col-lg-2 mb-4">
                                <div class="icon my-3 fs-2">
                                    <a href="clients" style="text-decoration: none; color:black;">
                                        <img src="../assets/clients.svg" alt="clients" style="width:100px; height:100px;">
                                    </a>
                                </div>
                                <h5 class="title-sm mt-4"><a href="clients" style="text-decoration: none; color:black;">User Management</a></h5>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
