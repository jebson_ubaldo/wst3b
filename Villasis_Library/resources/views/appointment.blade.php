@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/home" style="text-decoration: none;">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Book Appointment</li>
        </ol>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Set Appointment</h5>
                </div>
                <div class="card-body">
                    <div class="container mb-4">
                        <div class="col-md-12">
                            <form action = "/create" method = "post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <div class="form-group row">
                                    <div class="mb-2">
                                        <label for="service_availed">Service Availed</label>
                                        <select class="form-control mt-1 @error('service_availed') is-invalid @enderror" name="service_availed" id="service_availed" value="{{ old('service_availed') }}">
                                            <option value="" selected>- SELECT -</option>
                                            <option value="PSA">PSA</option>
                                            <option value="DFA">DFA</option>
                                            <option value="NBI">NBI</option>
                                            <option value="PRC">PRC</option>
                                            <option value="POEA">POEA</option>
                                            <option value="SSS">SSS</option>
                                            <option value="GSIS">GSIS</option>
                                            <option value="PAGIBIG">PAGIBIG</option>
                                        </select>
                                            @error('service_availed')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="date">Date</label>
                                        <input type="date" class="form-control mt-1 @error('appointment_date') is-invalid @enderror" id="appointment_date" name="appointment_date" value="{{ old('appointment_date') }}">
                                            @error('appointment_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-2"> 
                                        <label for="time">Time</label>
                                        <select class="form-control mt-1 @error('appointment_time') is-invalid @enderror" name="appointment_time" id="appointment_time" value="{{ old('appointment_time') }}">
                                            <option value="" selected>- SELECT -</option>
                                            <option value="8:00 am - 9:00 am">8:00 am - 9:00 am</option>
                                            <option value="9:00 am - 10:00 am">9:00 am - 10:00 am</option>
                                            <option value="10:00 am - 11:00 am">10:00 am - 11:00 am</option>
                                            <option value="11:00 am - 12:00 am">11:00 am - 12:00 pm</option>
                                            <option value="1:00 pm - 2:00 pm">1:00 pm - 2:00 pm</option>
                                            <option value="2:00 pm - 3:00 pm">2:00 pm - 3:00 pm</option>
                                            <option value="3:00 pm - 4:00 pm">3:00 pm - 4:00 pm</option>
                                            <option value="4:00 pm - 5:00 pm">4:00 pm - 5:00 pm</option>
                                        </select>
                                            @error('appointment_time')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <input type="hidden" id="status" name="status" value="1">
                                <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" id="name" name="name" value="{{ Auth::user()->name }}">
                                <input type="hidden" id="email" name="email" value="{{ Auth::user()->email }}">
                                <input type="hidden" id="accepted" name="accepted" value="1">
                                <button type="submit" class="btn btn-primary mt-4" name="set"><i class="fa fa-calendar" aria-hidden="true"></i> Set Appointment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>List of Appointments</h5></div>
                    <div class="card-body">
                        <div class="container mb-4">         
                            <div class="col-md-12">
                                <div class="card-body table-responsive">
                                <table class="table align-middle mb-0 bg-white" id="example">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>Service Availed</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($appointments as $appointment)
                                            <tr>
                                                <td>{{ $appointment->service_availed }}</td>
                                                <td>{{ date('F d, Y', strtotime($appointment->appointment_date)) }}</td>
                                                <td>{{ $appointment->appointment_time }}</td>
                                                <td>
                                                    @if($appointment->status == 0)
                                                        <span class="badge bg-warning">Pending</span>
                                                    @elseif ($appointment->status == 1)
                                                        <span class="badge bg-success">Approved</span>
                                                    @elseif ($appointment->status == 2)
                                                        <span class="badge bg-danger">Declined</span>
                                                    @endif   
                                                </td>
                                                <td>
                                                    @if($appointment->status == 0)
                                                        <!--<a href="/appointment-update/{{ $appointment->id }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
                                                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#cancelModal">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>
                                                    @else
                                                        <!--<button type="button" class="btn btn-outline-dark"><i class="fa fa-pencil" aria-hidden="true"></i></button>-->
                                                        <button type="button" class="btn btn-danger" disabled>
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>
                                                    @endif 
                                                </td>
                                            </tr>      
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Cancel Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action = "/home" method = "post">
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Cancel Appointment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mt-2 mb-2">
                    <div class="text-center">
                        <h5><p>Are you sure you want to cancel the appointment ?</p></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" name="set"><a href="/cancel/{{ $appointment->id}}" style="text-decoration:none; color:white;">Cancel Appointment</a></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection