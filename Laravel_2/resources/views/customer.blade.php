<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Customer</title>


        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            body {
                background-color: #edf2fc;
            }

            h1 {
                font-size: 44px;
                font-weight: bold;
                color: #6C6DF5;
            }

            i {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
    <section class="customer" id="customer">
        <div class="container-lg">
            <div class="row min-vh-100 align-items-center align-content-center">
                <div class="col-md-6 order-md-first mt-5 mt-md-0 ">
                    <div class="customer-text">
                        <h1>customer</h1>
                        <p>/ˈkʌstəmə/</p><br>
                        <p><i>noun</i></p>
                        <p>1. a person who buys goods or services from a shop or business.</p>
                        <p>2. a person of a specified kind with whom one has to deal.</p>
                    </div>
                </div>

                <div class="col-md-6 mt-5 mt-md-0">
                    <div class="customer-item shadow-sm p-4 rounded bg-white h-100">
                        <div class="form-group pt-4 pb-2">
                            <label for="custId" class="col-sm-4 control-label">Customer ID</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="custId" name="custId" value="{{$custId}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-2">
                            <label for="custName" class="col-sm-4 control-label">Customer Name</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="custName" name="custName" value="{{$custName}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-4">
                            <label for="custAddress" class="col-sm-4 control-label">Customer Address</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="custAddress" name="custAddress" value="{{$custAddress}}" required>
                            </div>
                        </div>
                    </div>
                    <footer class="bg-light text-center text-lg-start">
                        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                          Made with &#128153; by Jebson Ubaldo BSIT-3B
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </section>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
</html>