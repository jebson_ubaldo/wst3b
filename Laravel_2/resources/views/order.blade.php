<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Order</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Internal CSS -->
    <style>
        body {
            background-color: #edf2fc;
        }

        h1 {
            font-size: 44px;
            font-weight: bold;
            color: #6C6DF5;
        }

        i {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <section class="order" id="order"> 
        <div class="container-lg">
            <div class="row min-vh-100 align-items-center align-content-center">   
                
                <div class="col-md-6 order-md-first mt-5 mt-md-0 ">
                    <div class="item-text">
                        <h1>order</h1>
                        <p>/ˈɔːdə/</p><br>
                        <p><i>noun</i></p>
                        <p>1. a state in which everything is in its correct or appropriate place. </p>
                        <p>2. the arrangement or disposition of people or things in relation to each other according to a particular sequence, pattern, or method. </p>
                    </div>
                </div>


                <div class="col-md-6 mt-5 mt-md-0">
                    <div class="order-item shadow-sm p-4 rounded bg-white h-100">
                        <div class="form-group pt-4 pb-2">
                            <label for="custId" class="col-sm-4 control-label">Customer ID</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="custId" name="custId" value="{{$custId}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-2">
                            <label for="custName" class="col-sm-4 control-label">Customer Name</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="custName" name="custName" value="{{$custName}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-4">
                            <label for="orderNo" class="col-sm-4 control-label">Order No</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="orderNo" name="orderNo" value="{{$orderNo}}" required>
                            </div>
                        </div>

                        <div class="form-group pb-4">
                            <label for="orderDate" class="col-sm-4 control-label">Order Date</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="orderDate" name="orderDate" value="{{$orderDate}}" required>
                            </div>
                        </div>
                    </div>
                    <footer class="bg-light text-center text-lg-start">
                        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                          Made with &#128153; by Jebson Ubaldo BSIT-3B
                        </div>
                    </footer>
                </div>

            </div>
        </div>
    </section>
</body>
</html>