<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Activity 1</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            #click {
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 10px 12px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <form action="">
            <fieldset>
                <legend>Personal Information</legend>

                <label for="firstname">Firstname:</label><br>
                <input type="text" name="firstname" id="firstname" placeholder="Enter your firstname" required><br><br>

                <label for="lastname">Lastname:</label><br>
                <input type="text" name="lastname" id="lastname" placeholder="Enter your lastname" required><br><br>

                <label for="username">Username:</label><br>
                <input type="text" name="username" id="username" placeholder="Enter your username" required><br><br>

                <label for="password">Password:</label><br>
                <input type="password" name="password" id="password" style="border:5px solid red" placeholder="Enter your password" required><br><br>

                <textarea name="message" id="message" cols="50" rows="10">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                </textarea><br>

                <label for="birthdate">Birthdate:</label>
                <input type="date" name="birthdate" id="birthdate" required><br>

                <label for="town" style="color:red">Town</label>
                <select name="town" id="town" required>
                    <optgroup label="Urdaneta">
                        <option value="Nancayasan">Nancayasan</option>
                        <option value="San Vicente">San Vicente</option>
                    </optgroup>
                    <optgroup label="Villasis">
                        <option value="Poblacion">Poblacion</option>
                        <option value="Lipay">Lipay</option>
                    </optgroup>
                </select><br>

                <label for="browser">Browser:</label>
                <input list="browsers" name="browser" id="browser" required>
                <datalist id="browsers">
                <option value="Edge">
                    <option value="Firefox">
                    <option value="Chrome">
                    <option value="Opera">
                    <option value="Safari">
                </datalist><br>

                <input type="submit" id="click" class="btn btn-success" style="border:2px solid red" value="Click me">
                <input type="reset">
                <input type="button" value="pindot me" onclick="pindot_me()">
            </fieldset>
        </form>

        <script>
            function pindot_me(){
                alert("testme")
            }
        </script>
    </body>
</html>